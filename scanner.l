%{

#include <stdbool.h>
#include "ast.h"
#include "parser.tab.h"

#define CP_TT_PR        yylval.valor_lexico.literal_type = LT_STRING; \
                        yylval.valor_lexico.token_type = TT_RESERVED
#define CP_TT_SPECIAL   yylval.valor_lexico.literal_type = LT_STRING; \
                        yylval.valor_lexico.token_type = TT_SPECIAL
#define CP_TT_OP_COMPST yylval.valor_lexico.literal_type = LT_STRING; \
                        yylval.valor_lexico.token_type = TT_COMPOUND
#define CP_TT_IDENT     yylval.valor_lexico.literal_type = LT_STRING; \
                        yylval.valor_lexico.token_type = TT_IDENTIFIER
#define CP_TT_LITERAL   yylval.valor_lexico.token_type = TT_LITERAL

#define CP_STR_VAL      snprintf(yylval.valor_lexico.str_val, 510, "%s", yytext);
#define CP_QUOT_VAL     snprintf(yylval.valor_lexico.str_val, 510, "%s", &yytext[1]); \
                        int last_pos = strlen(yylval.valor_lexico.str_val) - 1; \
                        yylval.valor_lexico.str_val[last_pos] = '\0'

#define CP_STRING       yylval.valor_lexico.literal_type = LT_STRING; \
                        CP_QUOT_VAL; \
                        char tmp_buf[512] = ""; \
                        int buf_pos = 0; \
                        int len_str = strlen(yylval.valor_lexico.str_val); \
                        for (int i = 0; i < len_str; ++i) { \
                            if (yylval.valor_lexico.str_val[i] == '\\') { \
                                char c = yylval.valor_lexico.str_val[i+1]; \
                                switch (c) { \
                                    case '0': \
                                        tmp_buf[buf_pos++] = '\0'; \
                                        break; \
                                    case 'a': \
                                        tmp_buf[buf_pos++] = '\a'; \
                                        break; \
                                    case 'b': \
                                        tmp_buf[buf_pos++] = '\b'; \
                                        break; \
                                    case 'f': \
                                        tmp_buf[buf_pos++] = '\f'; \
                                        break; \
                                    case 'n': \
                                        tmp_buf[buf_pos++] = '\n'; \
                                        break; \
                                    case 'r': \
                                        tmp_buf[buf_pos++] = '\r'; \
                                        break; \
                                    case 't': \
                                        tmp_buf[buf_pos++] = '\t'; \
                                        break; \
                                    case 'v': \
                                        tmp_buf[buf_pos++] = '\v'; \
                                        break; \
                                    default: \
                                        tmp_buf[buf_pos++] = c; \
                                        break; \
                                } \
                                i++; \
                            } \
                            else { \
                                char c = yylval.valor_lexico.str_val[i]; \
                                tmp_buf[buf_pos++] = c; \
                            } \
                        } \
                        sprintf(yylval.valor_lexico.str_val, "%s", tmp_buf)
#define CP_CHAR         yylval.valor_lexico.literal_type = LT_CHAR; \
                        CP_QUOT_VAL; \
                        char c = yylval.valor_lexico.str_val[0]; \
                        yylval.valor_lexico.char_val = c
#define CP_INT          yylval.valor_lexico.literal_type = LT_INT; \
                        yylval.valor_lexico.int_val = atoi(yytext); \
                        CP_STR_VAL
#define CP_FLOAT        yylval.valor_lexico.literal_type = LT_FLOAT; \
                        yylval.valor_lexico.float_val = atof(yytext); \
                        CP_STR_VAL
#define CP_TRUE         yylval.valor_lexico.literal_type = LT_BOOL; \
                        yylval.valor_lexico.bool_val = true; \
                        yylval.valor_lexico.int_val = 1; \
                        CP_STR_VAL
#define CP_FALSE        yylval.valor_lexico.literal_type = LT_BOOL; \
                        yylval.valor_lexico.bool_val = false; \
                        yylval.valor_lexico.int_val = 0; \
                        CP_STR_VAL

#define CP_LINENO       yylval.valor_lexico.lineno = yylineno

%}

%option yylineno

%x COMMENT

INT                                 [0-9]+
ESCAPED                             \\.

%%

int                                 { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_INT; }
float                               { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_FLOAT; }
bool                                { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_BOOL; }
char                                { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_CHAR; }
string                              { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_STRING; }
if                                  { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_IF; }
then                                { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_THEN; }
else                                { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_ELSE; }
while                               { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_WHILE; }
do                                  { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_DO; }
input                               { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_INPUT; }
output                              { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_OUTPUT; }
return                              { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_RETURN; }
const                               { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_CONST; }
static                              { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_STATIC; }
foreach                             { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_FOREACH; }
for                                 { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_FOR; }
switch                              { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_SWITCH; }
case                                { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_CASE; }
break                               { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_BREAK; }
continue                            { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_CONTINUE; }
class                               { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_CLASS; }
private                             { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_PRIVATE; }
public                              { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_PUBLIC; }
protected                           { CP_LINENO; CP_TT_PR; CP_STR_VAL; return TK_PR_PROTECTED; }

","|";"|":"|"("|")"|"["|"]"         { CP_LINENO; CP_TT_SPECIAL; CP_STR_VAL; return yytext[0]; }
"{"|"}"|"+"|"-"|"|"|"?"|"*"         { CP_LINENO; CP_TT_SPECIAL; CP_STR_VAL; return yytext[0]; }
"/"|"<"|">"|"="|"!"|"&"|"%"         { CP_LINENO; CP_TT_SPECIAL; CP_STR_VAL; return yytext[0]; }
"#"|"^"|"."|"$"                     { CP_LINENO; CP_TT_SPECIAL; CP_STR_VAL; return yytext[0]; }

"<="                                { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_LE; }
">="                                { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_GE; }
"=="                                { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_EQ; }
"!="                                { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_NE; }
"&&"                                { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_AND; }
"||"                                { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_OR; }
"<<"                                { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_SL; }
">>"                                { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_SR; }
"%>%"                               { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_FORWARD_PIPE; }
"%|%"                               { CP_LINENO; CP_TT_OP_COMPST; CP_STR_VAL; return TK_OC_BASH_PIPE; }

{INT}                               { CP_LINENO; CP_TT_LITERAL; CP_INT; return TK_LIT_INT; }
{INT}\.[0-9]*([Ee][\+\-]?{INT})?    { CP_LINENO; CP_TT_LITERAL; CP_FLOAT; return TK_LIT_FLOAT; }
false                               { CP_LINENO; CP_TT_LITERAL; CP_FALSE; return TK_LIT_FALSE; }
true                                { CP_LINENO; CP_TT_LITERAL; CP_TRUE; return TK_LIT_TRUE; }
\'({ESCAPED}|[^\r\n\\\'])\'         { CP_LINENO; CP_TT_LITERAL; CP_CHAR; return TK_LIT_CHAR; }
\"({ESCAPED}|[^\r\n\\\"])*\"        { CP_LINENO; CP_TT_LITERAL; CP_STRING; return TK_LIT_STRING; }
[a-zA-Z_][a-zA-Z0-9_]*              { CP_LINENO; CP_TT_IDENT; CP_STR_VAL; return TK_IDENTIFICADOR; }

"/*"                                { BEGIN(COMMENT); }
<COMMENT>[^\*]+                     { /* consume everything that's not an '*' */ }
<COMMENT>[\*]+/[^\/]                { /* eat an '*' not followed by a '/' */ }
<COMMENT>\*\/                       { BEGIN(INITIAL); }
\/\/.*                              { /* in-line comment */ }

[\r\n\t ]+                          { /* blank spaces */ }

.                                   { CP_LINENO; CP_STR_VAL; return TOKEN_ERRO; }

%%

int get_line_number() { 
    return yylineno; 
}
