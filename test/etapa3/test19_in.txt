int f() {
  return 42;
}

int g() {
  return f();
}

int func1(int param1, const char param2) {
    {
      f() %|% g();

      f() %>% g();
    };
}
