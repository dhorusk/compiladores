#include <stdio.h>
#include "parser.tab.h" //arquivo gerado com bison -d parser.y
#include "queue.h"

TAILQ_HEAD(scope_s, ScopeEntry_t) scope;

void *arvore = NULL;
void descompila (void *arvore);
void libera (void *arvore);

int main (int argc, char **argv)
{
  // initialize scopes stack
  TAILQ_INIT(&scope);

  // create global scope
  ScopeEntry_t *global_scope = malloc(sizeof(ScopeEntry_t));
  global_scope->hash_table = NULL;
  TAILQ_INSERT_HEAD(&scope, global_scope, scopes);

  return yyparse();
  libera(arvore);
}
