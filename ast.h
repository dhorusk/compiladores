#ifndef __AST_H__
#define __AST_H__

#include <stdbool.h>
#include "uthash.h"
#include "queue.h"

typedef enum {
  TT_RESERVED,
  TT_SPECIAL,
  TT_COMPOUND,
  TT_IDENTIFIER,
  TT_LITERAL,
} TokenType_t;

typedef enum {
  LT_INT,
  LT_FLOAT,
  LT_STRING,
  LT_CHAR,
  LT_BOOL,
  LT_USER,
} LiteralType_t;

typedef enum {
  N_VARIABLE,
  N_CLASS_DECL,
  N_FUNC_DECL
} Nature_t;

typedef enum {
  AST_PROGRAM,
  AST_IDENTIFIER,
  AST_LITERAL,
  AST_CLASS,
  AST_CLASS_ATTRIBUTE,
  AST_FUNCTION,
  AST_IF_ELSE,
  AST_SWITCH,
  AST_CASE,
  AST_DO_WHILE,
  AST_WHILE,
  AST_FOR,
  AST_FOREACH,
  AST_BREAK,
  AST_CONTINUE,
  AST_ATTRIBUTION,
  AST_INPUT,
  AST_OUTPUT,
  AST_SHIFT,
  AST_RETURN,
  AST_BLOCK,
  AST_EXPRESSION_LIST,
  AST_COMMAND_LIST,
  AST_FOR_COMMAND_LIST,
  AST_PARAMETERS_LIST,
  AST_PIPE_COMMANDS,
  AST_PARAMETER,
  AST_EXPRESSION,
  AST_INITIALIZATION,
  AST_GLOBAL_DECLARATION,
  AST_LOCAL_DECLARATION,
  AST_INDEXED_VECTOR,
  AST_ACCESSED_CLASS,
  AST_PIPE_DOT
} NodeType_t;

typedef enum {
  ENC_PUBLIC,
  ENC_PRIVATE,
  ENC_PROTECTED
} Encapsulation_t;

typedef struct {
  int lineno;
  TokenType_t token_type;
  LiteralType_t literal_type;
  int int_val;
  float float_val;
  char char_val;
  char str_val[512];
  bool bool_val;
  void *node_val;
} LexicalValue_t;

typedef struct TreeNode_t {
  NodeType_t node_type;
  LexicalValue_t node_value;
  LiteralType_t infered_type;
  void *code_part;
  int register_number;
  int num_children;
  struct TreeNode_t **children;
} TreeNode_t;

typedef struct {
  bool is_vector;
  bool is_static;
  bool is_const;
  LiteralType_t literal_type;
  Nature_t nature;
  char class_name[512];
} Type_t;

typedef struct {
  char id[512];
  Type_t type;
  int bytes_size;
  bool is_const; // TODO: remove this later
} FunctionArgs_t;

typedef struct {
  Encapsulation_t encapsulation;
  char id[512];
  Type_t type;
  int bytes_size;
} ClassAttributes_t;

typedef struct {
  char id[512];
  int line_no;
  Type_t type_info;
  bool is_global;
  int start_label;
  int offset_from_base_reg;
  int bytes_size;
  int list_length;
  FunctionArgs_t *args_list;
  ClassAttributes_t *attributes_list;
  UT_hash_handle hh;
} HashTableEntry_t;

typedef struct ScopeEntry_t {
  HashTableEntry_t *hash_table;
  int base_register_offset;
  TAILQ_ENTRY(ScopeEntry_t) scopes;
} ScopeEntry_t;

typedef struct CodePart_t {
  char instruction[512];
  TAILQ_ENTRY(CodePart_t) codes;
} CodePart_t;

#endif // __AST_H__