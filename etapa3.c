/*
Função principal para realização da E3.

Este arquivo não pode ser modificado.
*/
#include <stdio.h>
#include "parser.tab.h" //arquivo gerado com bison -d parser.y
#include "queue.h"

TAILQ_HEAD(scope_s, ScopeEntry_t) scope;

void *arvore = NULL;
void descompila (void *arvore);
void libera (void *arvore);

int main (int argc, char **argv)
{
  // initialize scopes stack
  TAILQ_INIT(&scope);

  // create global scope
  ScopeEntry_t *global_scope = malloc(sizeof(ScopeEntry_t));
  global_scope->hash_table = NULL;
  TAILQ_INSERT_HEAD(&scope, global_scope, scopes);

  int ret = yyparse(); 
  descompila (arvore);
  libera(arvore);
  arvore = NULL;
  return ret;
}