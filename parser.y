%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "error.h"
#include "queue.h"
#include "uthash.h"

extern int yylineno;
extern TAILQ_HEAD(scope_s, ScopeEntry_t) scope;
extern void *arvore;
TAILQ_HEAD(code_s, CodePart_t) *code;
int current_global_offset = 0;
int current_register_number = 3; // reserve R0~R2 for function calls
int current_label_number = 2; // reserve L1 for main()
extern void descompila(void *tree);
extern void compila(void *tree);
extern void libera(void *tree);
void dump_code(struct code_s *code, int indent);
void dump_tree(TreeNode_t *node, int indent);
void _descompila(TreeNode_t *node, const char* separator);
void print_indent(int indent);
TreeNode_t *create_node(NodeType_t tipo);
TreeNode_t *create_literal_node(LexicalValue_t valor);
TreeNode_t *create_root();
void add_child(TreeNode_t *node, TreeNode_t *child_node);
void add_child_beginning(TreeNode_t *node, TreeNode_t *child_node);
void insert_child(TreeNode_t *node, TreeNode_t *child_node, bool beginning);
void set_static(HashTableEntry_t *entry, TreeNode_t *node);
void set_const(HashTableEntry_t *entry, TreeNode_t *node);
void set_vector(HashTableEntry_t *entry, TreeNode_t *node);
void set_entry_type_and_size(HashTableEntry_t *entry, LexicalValue_t literal_node, bool is_class_declaration, int vector_size);
void set_class_attr_type_and_size(ClassAttributes_t *class_attribute, LexicalValue_t literal_node);
void set_func_args_type_and_size(FunctionArgs_t *function_args, LexicalValue_t literal_node);
bool can_coerce(LiteralType_t from, LiteralType_t to);
LiteralType_t type_infer(LiteralType_t t1, LiteralType_t t2);
void set_encapsulation(ClassAttributes_t *class_attribute, LexicalValue_t literal_node);
void set_offset_from_base_reg(HashTableEntry_t *entry);
HashTableEntry_t *lookup_identifier(char *id, bool allow_return_null);
ClassAttributes_t *lookup_class_attr(char *class_type, char *class_attr);
HashTableEntry_t *lookup_identifier_current_scope(char *id, bool allow_return_null);
void check_function_args(TreeNode_t* node, bool has_infered_type);
void register_identifier(HashTableEntry_t *entry);
void dump_hashtable(HashTableEntry_t *htable);
void push_scope();
void pop_scope();
void dump_scopes();
int reserve_global_memory(int size);
int reserve_local_memory(int size);
int reserve_register();
int reserve_label();
struct code_s *create_code_segment();
struct code_s *add_instr_to_code_segment(struct code_s *segment, const char *instruction);
struct code_s *concat_code_segments(struct code_s *s1, struct code_s *s2);

int yylex(void);
void yyerror(char const *s);

%}

// include the header before creating the union
%code requires {
  #include "ast.h"
}

%union {
  LexicalValue_t valor_lexico;
  TreeNode_t *node;
}

%type <node> program program2 program3 decl_func primitive_value expression6
%type <node> simple_command simple_command2 simple_command3 simple_command4
%type <node> simple_command5 command command_block command_list opt_class_field
%type <node> expression expression_list expression_list_rest expression_value
%type <node> expression1 expression2 expression3 expression4 expression5
%type <node> numeric_value boolean_value identifier_value foreach_expr_list
%type <node> for_command_list identifier_value2 piped_function_call opt_const
%type <node> piped_function_calls_list function_call function_call_param_list
%type <node> function_call_param_list_rest expression_value2 opt_initialization
%type <node> foreach_expr_list_rest for_command_list_rest decl_func_param_list
%type <node> decl_func_param decl_func_param_list_rest opt_static decl_class_type
%type <node> decl_class_type_fields_list decl_class_type_fields_list_rest
%type <node> opt_encapsulation_type decl_class_type_field expression7 expression8
%type <node> expression9 expression10 expression11 func_command_block
%type <valor_lexico> TK_PR_INT TK_PR_FLOAT TK_PR_BOOL TK_PR_CHAR TK_PR_STATIC
%type <valor_lexico> TK_PR_STRING TK_LIT_INT TK_LIT_FLOAT TK_IDENTIFICADOR
%type <valor_lexico> TK_LIT_CHAR TK_LIT_STRING TK_LIT_TRUE TK_LIT_FALSE TK_PR_CLASS
%type <valor_lexico> TK_PR_WHILE TK_PR_DO TK_PR_FOREACH TK_PR_FOR TK_PR_SWITCH
%type <valor_lexico> TK_PR_IF TK_PR_THEN TK_OC_SL TK_OC_SR TK_OC_FORWARD_PIPE
%type <valor_lexico> '<' '>' TK_OC_LE TK_OC_GE TK_OC_BASH_PIPE TK_PR_CONST
%type <valor_lexico> TK_OC_EQ TK_OC_NE '?' ':' '|' '&' '^' TK_OC_AND TK_OC_OR
%type <valor_lexico> '+' '-' '*' '/' '%' '!' '#' '=' '.' primitive_type any_type
%type <valor_lexico> TK_PR_PRIVATE TK_PR_PROTECTED TK_PR_PUBLIC

%destructor { libera($$); } program2 program3 decl_func primitive_value
%destructor { libera($$); } simple_command simple_command2 simple_command3
%destructor { libera($$); } simple_command4 simple_command5 command command_list
%destructor { libera($$); } command_block opt_class_field opt_initialization
%destructor { libera($$); } expression_list_rest expression_value expression5
%destructor { libera($$); } expression1 numeric_value foreach_expr_list_rest
%destructor { libera($$); } identifier_value foreach_expr_list for_command_list
%destructor { libera($$); } identifier_value2 piped_function_call function_call
%destructor { libera($$); } piped_function_calls_list function_call_param_list
%destructor { libera($$); } function_call_param_list_rest expression_value2
%destructor { libera($$); } expression_list boolean_value for_command_list_rest
%destructor { libera($$); } expression expression2 expression3 expression4
%destructor { libera($$); } expression6 expression7 expression8 expression9
%destructor { libera($$); } expression10 expression11 opt_encapsulation_type
%destructor { libera($$); } opt_const opt_static func_command_block
%destructor { libera($$); } decl_func_param_list decl_func_param decl_class_type
%destructor { libera($$); } decl_func_param_list_rest decl_class_type_fields_list
%destructor { libera($$); } decl_class_type_fields_list_rest decl_class_type_field

%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR
%token TK_OC_FORWARD_PIPE
%token TK_OC_BASH_PIPE
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING
%token TK_IDENTIFICADOR
%token TOKEN_ERRO

%%

program:
  // at start of file
  // alternatives:
  //  1) class type declaration
  //  2) global variable declaration
  //  3) function declaration
  // if we read TK_PR_CLASS, then it,s a class declaration
  // if we read a primitive type, then it,s a function declaration
  // if we read TK_PR_STATIC, then it,s a static function declaration
  // if we read TK_IDENTIFICADOR, we must decide between global variable and function declaration
  program decl_class_type {
    // class declaration

    add_child($$, $2);

    if ($2->node_type == AST_CLASS) {
      HashTableEntry_t *entry = calloc(1, sizeof(HashTableEntry_t));
      entry->line_no = yylineno;

      // AST_CLASS children are always num_attributes + 1 (identifier)
      entry->list_length = $2->num_children - 1;

      // set everything else
      snprintf(entry->id, 510, "%s", $2->children[0]->node_value.str_val);

      if (entry->list_length > 0) {
        ClassAttributes_t* class_attr_list = calloc(1, sizeof(ClassAttributes_t) * entry->list_length);
        for (int i = 1; i <= entry->list_length; i++) {
          int last_child = $2->children[i]->num_children;
          snprintf(class_attr_list[i - 1].id, 510, "%s", $2->children[i]->children[last_child - 1]->node_value.str_val);
          set_class_attr_type_and_size(&(class_attr_list[i - 1]), $2->children[i]->children[last_child - 2]->node_value);
          set_encapsulation(&(class_attr_list[i-1]), $2->children[i]->children[0]->node_value);
        }
        // set attributes list
        entry->attributes_list = class_attr_list;
      }

      // set class declaration type and calculate class size
      set_entry_type_and_size(entry, $2->children[0]->node_value, true, 1);

      // add to current scope hashtable
      register_identifier(entry);
    }
  }
| program primitive_type TK_IDENTIFICADOR '(' decl_func_param_list ')' {
    HashTableEntry_t *entry = calloc(1, sizeof(HashTableEntry_t));
    entry->line_no = yylineno;
    entry->list_length = $5->num_children;
    entry->type_info.is_static = false;
    entry->type_info.is_vector = false;
    entry->type_info.is_const = false;

    // set everything else
    snprintf(entry->id, 510, "%s", $3.str_val);
    set_entry_type_and_size(entry, $2, false, 1);
    entry->type_info.nature = N_FUNC_DECL;
    if (strcmp($3.str_val, "main") == 0)
      entry->start_label = 1;
    else
      entry->start_label = reserve_label();

    if (entry->list_length > 0) {
      FunctionArgs_t *args_list = calloc(1, sizeof(FunctionArgs_t) * entry->list_length);
      for (int i = 0; i < entry->list_length; i++) {
        int last_child = $5->children[i]->num_children;
        snprintf(args_list[i].id, 510, "%s", $5->children[i]->children[last_child - 1]->node_value.str_val);
        set_func_args_type_and_size(&(args_list[i]), $5->children[i]->children[last_child - 2]->node_value);

        if (last_child > 2) {
          args_list[i].is_const = true;
        }
      }
      // set attributes list
      entry->args_list = args_list;
    }

    // add to current scope hashtable
    register_identifier(entry);

    // populate code segment with the needed instructions
    char instr[512];

    snprintf(instr, 510, "l%d: // function prologue\n", entry->start_label);
    $1->code_part = add_instr_to_code_segment($1->code_part, instr);

    if (strcmp(entry->id, "main") != 0) {
      snprintf(instr, 510, "i2i rsp => rfp // function prologue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);
    }

  } decl_func {
    // function declaration

    add_child_beginning($8, $5);

    $$ = create_node(AST_IDENTIFIER);
    add_child($$, create_literal_node($3));
    add_child_beginning($8, $$);

    $$ = create_literal_node($2);
    add_child_beginning($8, $$);

    add_child($1, $8);

    // populate code segment with the needed instructions
    if (strcmp($3.str_val, "main") != 0) {
      char instr[512];

      snprintf(instr, 510, "loadAI rfp, 0 => r0 // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "loadAI rfp, 4 => r1 // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "loadAI rfp, 8 => r2 // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "i2i r1 => rsp // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "i2i r2 => rfp // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "jump -> r0 // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);
    }

    $$ = $1;
  }
| program TK_PR_STATIC any_type TK_IDENTIFICADOR '(' decl_func_param_list ')' {
    HashTableEntry_t *entry = calloc(1, sizeof(HashTableEntry_t));
    entry->line_no = yylineno;
    entry->list_length = $6->num_children;
    entry->type_info.is_static = true;
    entry->type_info.is_vector = false;
    entry->type_info.is_const = false;

    // set everything else
    snprintf(entry->id, 510, "%s", $4.str_val);
    set_entry_type_and_size(entry, $3, false, 1);
    entry->type_info.nature = N_FUNC_DECL;
    if (strcmp(entry->id, "main") == 0)
      entry->start_label = 1;
    else
      entry->start_label = reserve_label();

    if (entry->list_length > 0) {
      FunctionArgs_t* args_list = calloc(1, sizeof(FunctionArgs_t) * entry->list_length);
      for (int i = 0; i < entry->list_length; i++) {
        int last_child = $6->children[i]->num_children;
        snprintf(args_list[i].id, 510, "%s", $6->children[i]->children[last_child - 1]->node_value.str_val);
        set_func_args_type_and_size(&(args_list[i]), $6->children[i]->children[last_child - 2]->node_value);
        if (last_child > 2) {
          args_list[i].is_const = true;
        }

      }
      // set attributes list
      entry->args_list = args_list;
    }

    // add to current scope hashtable
    register_identifier(entry);

    // populate code segment with the needed instructions
    char instr[512];

    snprintf(instr, 510, "l%d: // function prologue\n", entry->start_label);
    $1->code_part = add_instr_to_code_segment($1->code_part, instr);

    if (strcmp($4.str_val, "main") != 0) {
      snprintf(instr, 510, "i2i rsp => rfp // function prologue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "addI rsp, %d => rsp // function prologue\n",
               16 + entry->list_length * 4);
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);
    }

  } decl_func {
    // function declaration

    add_child_beginning($9, $6);

    $$ = create_node(AST_IDENTIFIER);
    add_child($$, create_literal_node($4));
    add_child_beginning($9, $$);

    $$ = create_literal_node($3);
    add_child_beginning($9, $$);

    $$ = create_literal_node($2);
    add_child_beginning($9, $$);

    add_child($1, $9);

    // populate code segment with the needed instructions
    if (strcmp($4.str_val, "main") != 0) {
      char instr[512];

      snprintf(instr, 510, "loadAI rfp, 0 => r0 // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "loadAI rfp, 4 => r1 // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "loadAI rfp, 8 => r2 // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "i2i r1, rsp // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "i2i r2, rfp // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);

      snprintf(instr, 510, "jump -> r0 // function epilogue\n");
      $1->code_part = add_instr_to_code_segment($1->code_part, instr);
    }

    $$ = $1;
  }
| program TK_IDENTIFICADOR program2 {
    // global var or function declaration

    if ($3 != NULL && $3->num_children > 0
        && $3->children[0]->node_type == AST_INDEXED_VECTOR)
    {
      $$ = create_node(AST_IDENTIFIER);
      add_child($$, create_literal_node($2));
      add_child_beginning($3->children[0], $$);
    }
    else {
      add_child_beginning($3, create_literal_node($2));
    }
    add_child($1, $3);

    if ($3->node_type == AST_GLOBAL_DECLARATION) {
      HashTableEntry_t *entry = calloc(1, sizeof(HashTableEntry_t));
      entry->line_no = yylineno;
      entry->list_length = 0;

      // set everything else
      snprintf(entry->id, 510, "%s", $2.str_val);
      set_static(entry, $3);
      set_vector(entry, $3);
      set_const(entry, $3);
      if (entry->type_info.is_vector) {
        int vector_size = $3->children[0]->children[1]->node_value.int_val;
        set_entry_type_and_size(entry, $3->children[$3->num_children-1]->node_value, false, vector_size);
      }
      else
        set_entry_type_and_size(entry, $3->children[$3->num_children-1]->node_value, false, 1);

      set_offset_from_base_reg(entry);

      // add to current scope hashtable
      register_identifier(entry);

      // populate code segment with the needed instructions
      char instr[512];
      int temp_register = reserve_register();

    }
    else if ($3->node_type == AST_FUNCTION) {
      HashTableEntry_t *entry = calloc(1, sizeof(HashTableEntry_t));
      entry->line_no = yylineno;
      entry->list_length = $3->children[$3->num_children-1]->num_children;
      entry->type_info.is_static = false;
      entry->type_info.is_vector = false;
      entry->type_info.is_const = false;

      // set everything else
      snprintf(entry->id, 510, "%s", $3->children[1]->node_value.str_val);
      set_entry_type_and_size(entry, $3->children[0]->node_value, false, 1);
      entry->type_info.nature = N_FUNC_DECL;
      if (strcmp(entry->id, "main") == 0)
        entry->start_label = 1;
      else
        entry->start_label = reserve_label();

      // add to current scope hashtable
      register_identifier(entry);

    }

    $$ = $1;
  }
| %empty {
    // always the first action to be executed
    $$ = create_root();

    arvore = $$;

    // populate code segment with the needed instructions
    char instr[512];

    snprintf(instr, 510, "loadI 1024 -> rfp // entry point\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "loadI 1024 -> rsp // entry point\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "loadI 0 -> rbss // entry point\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "jumpI -> l1 // entry point\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    code = $$->code_part;
  }
;

program2:
  // already read TK_IDENTIFICADOR
  // decide between global variable and function declaration
  // if we read an open bracket, then it,s a global variable declaration
  // if we read TK_PR_STATIC, then it,s a global variable declaration
  // if we read a primitive type, then it,s a global variable declaration
  // if we read a TK_IDENTIFICADOR, we must decide between a class-typed global variable and function declaration
  '[' TK_LIT_INT ']' opt_static any_type ';' {
    $$ = create_node(AST_GLOBAL_DECLARATION);

    TreeNode_t *node = create_node(AST_INDEXED_VECTOR);
    add_child(node, create_literal_node($2));
    add_child($$, node);

    if ($4 != NULL)
      add_child($$, $4);

    add_child($$, create_literal_node($5));
  }
| TK_PR_STATIC any_type ';' {
    $$ = create_node(AST_GLOBAL_DECLARATION);
    add_child($$, create_literal_node($1));
    add_child($$, create_literal_node($2));
  }
| primitive_type ';' {
    $$ = create_node(AST_GLOBAL_DECLARATION);
    add_child($$, create_literal_node($1));
  }
| TK_IDENTIFICADOR program3 {
    add_child_beginning($2, create_literal_node($1));
    $$ = $2;
  }
;

program3:
  // already read TK_IDENTIFICADOR TK_IDENTIFICADOR
  // decide between class-typed global variable and function declaration
  // if we read a semicolon, then it,s a class-typed global variable declaration
  // if we read an open parenthesis, then it`s a class-typed function declaration
  ';' {
    $$ = create_node(AST_GLOBAL_DECLARATION);
  }
| '(' decl_func_param_list ')' decl_func {
    add_child_beginning($4, $2);
    $$ = $4;
  }
;


decl_class_type:
  TK_PR_CLASS TK_IDENTIFICADOR '[' decl_class_type_fields_list ']' ';' {
    add_child_beginning($4, create_literal_node($2));
    $$ = $4;
  }
;

decl_class_type_fields_list:
  decl_class_type_field decl_class_type_fields_list_rest {
    add_child_beginning($2, $1);
    $$ = $2;
  }
;

decl_class_type_fields_list_rest:
  %empty {
    $$ = create_node(AST_CLASS);
  }
| decl_class_type_fields_list_rest ':' decl_class_type_field {
    add_child($1, $3);
    $$ = $1;
  }
;

decl_class_type_field:
  opt_encapsulation_type primitive_type TK_IDENTIFICADOR {
    $$ = create_node(AST_CLASS_ATTRIBUTE);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, create_literal_node($3));
  }
;


decl_func:
  func_command_block {
    $$ = create_node(AST_FUNCTION);
    add_child($$, $1);
  }
;

decl_func_param:
  opt_const any_type TK_IDENTIFICADOR {
    $$ = create_node(AST_PARAMETER);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, create_literal_node($3));
  }
;

decl_func_param_list:
  %empty {
    $$ = create_node(AST_PARAMETERS_LIST);
  }
| decl_func_param decl_func_param_list_rest {
    add_child_beginning($2, $1);
    $$ = $2;
  }
;

decl_func_param_list_rest:
  %empty {
    $$ = create_node(AST_PARAMETERS_LIST);
  }
| decl_func_param_list_rest ',' decl_func_param {
    add_child($1, $3);
    $$ = $1;
  }
;


command:
  simple_command {
    $$ = $1;
    if ($1->node_type == AST_LOCAL_DECLARATION) {
      HashTableEntry_t *entry = calloc(1, sizeof(HashTableEntry_t));
      entry->line_no = yylineno;
      entry->type_info.is_vector = false; // not allowed
      entry->list_length = 0;

      // set everything else
      snprintf(entry->id, 510, "%s", $1->children[$1->num_children-1]->children[0]->node_value.str_val);
      set_static(entry, $1);
      set_const(entry, $1);
      set_entry_type_and_size(entry, $1->children[$1->num_children-2]->node_value, false, 1);
      set_offset_from_base_reg(entry);

      // add to current scope hashtable
      register_identifier(entry);
    }
    else if ($1->node_type == AST_INITIALIZATION) {
      HashTableEntry_t *entry = calloc(1, sizeof(HashTableEntry_t));
      entry->line_no = yylineno;
      entry->type_info.is_vector = false; // not allowed
      entry->list_length = 0;

      // set everything else
      snprintf(entry->id, 510, "%s", $1->children[$1->num_children-2]->children[0]->node_value.str_val);
      set_static(entry, $1);
      set_const(entry, $1);
      set_entry_type_and_size(entry, $1->children[$1->num_children-3]->node_value, false, 1);
      set_offset_from_base_reg(entry);

      // check types
      TreeNode_t *rvalue = $1->children[$1->num_children-1];
      LiteralType_t rvalue_type;
      LiteralType_t lvalue_type = entry->type_info.literal_type;

      if (rvalue->node_type == AST_LITERAL) {
        rvalue_type = rvalue->node_value.literal_type;
      }
      else if (rvalue->node_type == AST_IDENTIFIER) {
        HashTableEntry_t *tmp = lookup_identifier(rvalue->children[0]->node_value.str_val, false);
        rvalue_type = tmp->type_info.literal_type;
      }

      if (rvalue_type != lvalue_type
          && !can_coerce(rvalue_type, lvalue_type))
      {
        printf("Line %d: Incompatible types in initialization of \"%s\".\n", yylineno, entry->id);
        if (rvalue_type == LT_STRING)
          exit(ERR_STRING_TO_X);
        else if (rvalue_type == LT_CHAR)
          exit(ERR_CHAR_TO_X);
        else if (rvalue_type == LT_USER)
          exit(ERR_USER_TO_X);
        else
          exit(ERR_WRONG_TYPE);
      }

      // set string length
      if (lvalue_type == LT_STRING) {
        if (rvalue->node_type == AST_LITERAL) {
          entry->bytes_size = strlen(rvalue->node_value.str_val) + 1;
        }
        else if (rvalue->node_type == AST_IDENTIFIER) {
          HashTableEntry_t *tmp = lookup_identifier(rvalue->children[0]->node_value.str_val, false);
          entry->bytes_size = tmp->bytes_size;
        }
      }

      // populate code segment with the needed instructions
      char instr[512];
      int temp_register = reserve_register();

      snprintf(instr, 510, "loadI %d => r%d // initialization\n",
               $1->children[2]->node_value.int_val, temp_register);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      snprintf(instr, 510, "storeAI r%d => rfp, %d // initialization\n",
               temp_register, entry->offset_from_base_reg);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      // add to current scope hashtable
      register_identifier(entry);
    }
    else if ($1->node_type == AST_ATTRIBUTION) {
      HashTableEntry_t *lvalue;
      LiteralType_t lvalue_type;

      if ($1->children[0]->node_type == AST_IDENTIFIER) {
        HashTableEntry_t *tmp = lookup_identifier($1->children[0]->children[0]->node_value.str_val, false);

        if (tmp->type_info.is_vector) {
          printf("Line %d: Missing index for vector \"%s\".\n", yylineno, tmp->id);
          exit(ERR_VECTOR);
        }

        if (tmp->type_info.nature == N_FUNC_DECL) {
          printf("Line %d: \"%s\" is a function.\n", yylineno, tmp->id);
          exit(ERR_FUNCTION);
        }

        if (tmp->type_info.nature == N_CLASS_DECL) {
          printf("Line %d: \"%s\" is a class.\n", yylineno, tmp->id);
          exit(ERR_USER);
        }

        lvalue = tmp;
        lvalue_type = tmp->type_info.literal_type;
      }
      else if ($1->children[0]->node_type == AST_INDEXED_VECTOR) {
        HashTableEntry_t *tmp = lookup_identifier($1->children[0]->children[0]->children[0]->node_value.str_val, false);

        if (tmp->type_info.nature == N_FUNC_DECL) {
          printf("Line %d: \"%s\" is a function.\n", yylineno, tmp->id);
          exit(ERR_FUNCTION);
        }

        if (tmp->type_info.nature == N_CLASS_DECL) {
          printf("Line %d: \"%s\" is a class.\n", yylineno, tmp->id);
          exit(ERR_USER);
        }

        if (!tmp->type_info.is_vector) {
          printf("Line %d: Indexed variable \"%s\" is NOT a vector.\n", yylineno, tmp->id);
          exit(ERR_VARIABLE);
        }

        lvalue = tmp;
        lvalue_type = tmp->type_info.literal_type;
      }
      else if ($1->children[0]->node_type == AST_ACCESSED_CLASS) {
        HashTableEntry_t *class_entry;

        if ($1->children[0]->children[0] != NULL
            && $1->children[0]->children[0]->node_type == AST_INDEXED_VECTOR)
        {
          class_entry = lookup_identifier($1->children[0]->children[0]->children[0]->children[0]->node_value.str_val, false);
        }
        else {
          class_entry = lookup_identifier($1->children[0]->children[0]->children[0]->node_value.str_val, false);
        }

        if (class_entry->type_info.nature == N_FUNC_DECL) {
          printf("Line %d: \"%s\" is a function.\n", yylineno, class_entry->id);
          exit(ERR_FUNCTION);
        }

        if (class_entry->type_info.literal_type != LT_USER) {
          printf("Line %d: Accessed variable \"%s\" is NOT a class.\n", yylineno, class_entry->id);
          exit(ERR_VARIABLE);
        }

        ClassAttributes_t *class_attr_entry;

        if ($1->children[0]->children[0] != NULL
            && $1->children[0]->children[0]->node_type == AST_INDEXED_VECTOR)
        {
          class_attr_entry = lookup_class_attr($1->children[0]->children[0]->children[0]->children[0]->node_value.str_val, $1->children[0]->children[1]->children[0]->node_value.str_val);
        }
        else {
          class_attr_entry = lookup_class_attr($1->children[0]->children[0]->children[0]->node_value.str_val, $1->children[0]->children[1]->children[0]->node_value.str_val);
        }

        lvalue = class_entry;
        lvalue_type = class_attr_entry->type.literal_type;
      }

      TreeNode_t *rvalue = $1->children[$1->num_children-1];
      LiteralType_t rvalue_type = $1->children[1]->infered_type;

      if (rvalue_type != lvalue_type
          && !can_coerce(rvalue_type, lvalue_type))
      {
        printf("Line %d: Incompatible types in attribution.\n", yylineno);
        if (rvalue_type == LT_STRING)
          exit(ERR_STRING_TO_X);
        else if (rvalue_type == LT_CHAR)
          exit(ERR_CHAR_TO_X);
        else if (rvalue_type == LT_USER)
          exit(ERR_USER_TO_X);
        else
          exit(ERR_WRONG_TYPE);
      }

      // check string length
      if (lvalue_type == LT_STRING) {
        if (rvalue->node_type == AST_LITERAL) {
          if (lvalue->bytes_size == -1) {
            lvalue->bytes_size = strlen(rvalue->node_value.str_val) + 1;
          }
          else if (lvalue->bytes_size < strlen(rvalue->node_value.str_val) + 1) {
            printf("Line %d: String length exceeds variable size.\n", yylineno);
            exit(ERR_STRING_TO_X);
          }
        }
        else if (rvalue->node_type == AST_IDENTIFIER) {
          HashTableEntry_t *tmp = lookup_identifier(rvalue->children[0]->node_value.str_val, false);

          if (lvalue->bytes_size == -1) {
            lvalue->bytes_size = tmp->bytes_size;
          }
          else if (lvalue->bytes_size < tmp->bytes_size + 1)
          {
            printf("Line %d: String length exceeds variable size.\n", yylineno);
            exit(ERR_STRING_TO_X);
          }
        }
      }

      // populate code segment with the needed instructions
      char instr[512];

      if (lvalue->is_global) {
        snprintf(instr, 510, "storeAI r%d => rbss, %d // attribution\n",
                 $1->children[1]->register_number, lvalue->offset_from_base_reg);
        $$->code_part = add_instr_to_code_segment($$->code_part, instr);
      }
      else {
        snprintf(instr, 510, "storeAI r%d => rfp, %d // attribution\n",
                 $1->children[1]->register_number, lvalue->offset_from_base_reg);
        $$->code_part = add_instr_to_code_segment($$->code_part, instr);
      }
    }
    else if ($1->node_type != AST_IF_ELSE
             && $1->node_type != AST_WHILE
             && $1->node_type != AST_DO_WHILE
             && $1->node_type != AST_RETURN
             && $1->node_type != AST_PIPE_COMMANDS)
    {
      // free unused code
      if ($$->code_part != NULL) {
        struct code_s *tmp_code = $$->code_part;
        while (!TAILQ_EMPTY(tmp_code)) {
          CodePart_t *code_part = TAILQ_FIRST(tmp_code);
          TAILQ_REMOVE(tmp_code, code_part, codes);
          free(code_part);
        }

        free($$->code_part);
        $$->code_part = NULL;
      }
    }
  }
| command_block {
    $$ = $1;
  }
;

command_block:
  start_block command_list end_block {
    $$ = create_node(AST_BLOCK);
    add_child($$, $2);
  }
;

func_command_block:
  func_start_block command_list end_block {
    $$ = create_node(AST_BLOCK);
    add_child($$, $2);
  }
;

start_block:
  '{' {
    push_scope();
    // DEBUG: dump all scopes
    // dump_scopes();
  }
;

func_start_block:
  '{' {
    push_scope();

    // figure out which function we're in
    // should be the last entry in the global scope
    ScopeEntry_t *global_scope = TAILQ_LAST(&scope, scope_s);
    HashTableEntry_t *last_entry = global_scope->hash_table;

    while(last_entry->hh.next != NULL)
      last_entry = last_entry->hh.next;

    // if we found the function
    if (last_entry->type_info.nature == N_FUNC_DECL) {
      // reserve space for the activation register
      if (strcmp(last_entry->id, "main") != 0)
        reserve_local_memory(3 * 4);

      // register all the params
      for (int i = 0; i < last_entry->list_length; i++) {
        HashTableEntry_t *entry = calloc(1, sizeof(HashTableEntry_t));
        entry->line_no = yylineno;
        entry->type_info.is_vector = false; // not allowed
        entry->type_info.is_static = false;
        entry->list_length = 0;

        // set everything else
        snprintf(entry->id, 510, "%s", last_entry->args_list[i].id);
        entry->type_info.nature = last_entry->args_list[i].type.nature;
        entry->type_info.literal_type = last_entry->args_list[i].type.literal_type;
        snprintf(entry->type_info.class_name, 510, "%s", last_entry->args_list[i].type.class_name);
        entry->bytes_size = last_entry->args_list[i].bytes_size;
        entry->type_info.is_const = last_entry->args_list[i].is_const;
        set_offset_from_base_reg(entry);
        register_identifier(entry);
      }

      // reserve space for the return value and static parent
      if (strcmp(last_entry->id, "main") != 0)
        reserve_local_memory(2 * 4);;
    }

    // DEBUG: dump all scopes
    // dump_scopes();
  }
;

end_block:
  '}' {
    pop_scope();
    // DEBUG: dump all scopes
    dump_scopes();
  }
;

command_list:
  %empty {
    $$ = create_node(AST_COMMAND_LIST);
  }
| command_list command ';' {
    add_child($1, $2);
    $$ = $1;
  }
| command_list TK_PR_CASE TK_LIT_INT ':' {
    $$ = create_node(AST_CASE);

    TreeNode_t *args = create_node(AST_LITERAL);
    args->node_value = $3;
    add_child($$, args);

    add_child($1, $$);
    $$ = $1;
  }
| command_list TK_PR_OUTPUT expression_list ';' {
    $$ = create_node(AST_OUTPUT);
    add_child($$, $3);

    for (int i = 0; i < $3->num_children; i++) {
      if ($3->children[i]->node_type == AST_LITERAL
          && $3->children[i]->node_value.literal_type != LT_STRING
          && !can_coerce($3->children[i]->node_value.literal_type, LT_INT))
      {
        printf("Line %d: Output parameter is NOT a string or an arithmetic expression.\n", yylineno);
        exit(ERR_WRONG_PAR_OUTPUT);
      }
      else if ($3->children[i]->node_type == AST_EXPRESSION
               && !can_coerce($3->children[i]->node_value.literal_type, LT_INT))
      {
        printf("Line %d: Output expression is NOT arithmetic.\n", yylineno);
        exit(ERR_WRONG_PAR_OUTPUT);
      }
    }

    add_child($1, $$);
    $$ = $1;
  }
;

simple_command:
  // at start of simple_command
  // alternatives:
  //  1) local variable declaration
  //  2) attribution
  //  3) shift command
  //  4) I/O
  //  5) return statement
  //  6) break/continue
  //  7) while/do-while
  //  8) switch
  //  9) if/if-else statement
  // if we read a primitive type, then it,s a local variable declaration
  // if we read TK_PR_INPUT, then it,s an input command
  // if we read TK_PR_RETURN, then it,s a return statement
  // if we read TK_PR_BREAK or TK_PR_CONTINUE, then it,s a control flow command
  // if we read TK_PR_WHILE or TK_PR_DO, then it,s a control flow command
  // if we read TK_PR_SWITCH, then it,s a selection command
  // if we read TK_PR_FOREACH or a TK_PR_FOR, then it,s a repetition control flow command
  // if we read TK_IDENTIFICADOR, we must decide between class-type local variable declaration, attribution, and function call
  // if we read TK_PR_STATIC or TK_PR_CONST, we must decide between a class-type and a primitive type variable declaration
  // if we read TK_PR_IF, we must decide between having or not an else statement
  primitive_type TK_IDENTIFICADOR opt_initialization {
    if ($3 != NULL) {
      $$ = create_node(AST_IDENTIFIER);
      add_child($$, create_literal_node($2));
      add_child_beginning($3, $$);

      $$ = create_literal_node($1);
      add_child_beginning($3, $$);

      $$ = $3;
    }
    else {
      $$ = create_node(AST_LOCAL_DECLARATION);
      add_child($$, create_literal_node($1));

      TreeNode_t *node = create_node(AST_IDENTIFIER);
      add_child(node, create_literal_node($2));
      add_child($$, node);
    }
  }
| TK_PR_INPUT expression {
    $$ = create_node(AST_INPUT);
    add_child($$, $2);

    if ($2->node_type != AST_IDENTIFIER
        && $2->node_type != AST_INDEXED_VECTOR
        && $2->node_type != AST_ACCESSED_CLASS)
    {
      printf("Line %d: Input parameter isn't a variable.\n", yylineno);
      exit(ERR_WRONG_PAR_INPUT);
    }
  }
| TK_PR_RETURN expression {
    $$ = create_node(AST_RETURN);
    add_child($$, $2);

    // figure out which function we're in
    // should be the last entry in the global scope
    ScopeEntry_t *global_scope = TAILQ_LAST(&scope, scope_s);
    HashTableEntry_t *last_entry = global_scope->hash_table;

    while(last_entry->hh.next != NULL)
      last_entry = last_entry->hh.next;

    if (!can_coerce($2->infered_type, last_entry->type_info.literal_type)) {
      printf("Line %d: Return parameter's type does NOT match function's declared type.\n",
             yylineno);
      exit(ERR_WRONG_PAR_RETURN);
    }

    // populate code segment with the needed instructions
    char instr[512];

    snprintf(instr, 510, "storeAI r%d => rfp, %d // return\n",
             $2->register_number, 12 + last_entry->list_length * 4);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "loadAI rfp, 0 => r0 // function epilogue\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "loadAI rfp, 4 => r1 // function epilogue\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "loadAI rfp, 8 => r2 // function epilogue\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "i2i r1 => rsp // function epilogue\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "i2i r2 => rfp // function epilogue\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "jump -> r0 // function epilogue\n");
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
  }
| TK_PR_BREAK {
    $$ = create_node(AST_BREAK);
  }
| TK_PR_CONTINUE {
    $$ = create_node(AST_CONTINUE);
  }
| TK_PR_WHILE '(' expression ')' TK_PR_DO command_block {
    $$ = create_node(AST_WHILE);

    int first_label = reserve_label();
    int second_label = reserve_label();
    int third_label = reserve_label();
    char instr[512];

    // Expression evaluation
    snprintf(instr, 510, "l%d: ", first_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    add_child($$, $3);

    snprintf(instr, 510, "cbr r%d -> l%d, l%d // while\n",
             $3->register_number, second_label, third_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
    snprintf(instr, 510, "l%d: ", second_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    // Command block
    add_child($$, $6);

    // Return to expression evaluation
    snprintf(instr, 510, "jumpI -> l%d // while\n", first_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    // Leaving while command
    snprintf(instr, 510, "l%d: ", third_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    if (!can_coerce($3->infered_type, LT_BOOL)) {
      printf("Line %d: If condition must be a boolean expression.\n", yylineno);
      exit(ERR_WRONG_TYPE);
    }
  }
| TK_PR_DO command_block TK_PR_WHILE '(' expression ')' {
    $$ = create_node(AST_DO_WHILE);

    int first_label = reserve_label();
    int second_label = reserve_label();
    char instr[512];

    // Command block (executed once regardless of the expression evaluation)
    snprintf(instr, 510, "l%d: ", first_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
    add_child($$, $2);

    // Expression evaluation
    add_child($$, $5);
    snprintf(instr, 510, "cbr r%d -> l%d, l%d // do-while\n",
             $5->register_number, first_label, second_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    // Leave do-while command
    snprintf(instr, 510, "l%d: ", second_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    if (!can_coerce($5->infered_type, LT_BOOL)) {
      printf("Line %d: Do-while condition must be a boolean expression.\n", yylineno);
      exit(ERR_WRONG_TYPE);
    }
  }
| TK_PR_FOREACH '(' identifier_value ':' foreach_expr_list ')' command_block {
    $$ = create_node(AST_FOREACH);

    add_child($$, $3);
    add_child($$, $5);
    add_child($$, $7);

  }
| TK_PR_FOR '(' {
    push_scope();
  } for_command_list ':' expression ':' for_command_list ')' command_block {
    $$ = create_node(AST_FOR);

    add_child($$, $4);
    add_child($$, $6);
    add_child($$, $8);
    add_child($$, $10);

    if (!can_coerce($6->infered_type, LT_BOOL)) {
      printf("Line %d: For condition must be a boolean expression.\n", yylineno);
      exit(ERR_WRONG_TYPE);
    }

    pop_scope();
  }
| TK_PR_SWITCH '(' expression ')' command_block {
    $$ = create_node(AST_SWITCH);

    add_child($$, $3);
    add_child($$, $5);

    if (!can_coerce($3->infered_type, LT_INT)) {
      printf("Line %d: Switch value must be an arithmetic expression.\n", yylineno);
      exit(ERR_WRONG_TYPE);
    }
  }
| TK_IDENTIFICADOR simple_command2 {
    if ($2 != NULL && $2->node_type == AST_LOCAL_DECLARATION) {
      add_child_beginning($2, create_literal_node($1));
    }
    else if ($2 != NULL && $2->node_type == AST_PIPE_COMMANDS) {
      TreeNode_t *node = create_node(AST_IDENTIFIER);
      add_child(node, create_literal_node($1));
      add_child_beginning($2, node);
      check_function_args($2, false);

      // get the topmost scope
      ScopeEntry_t *top_scope = TAILQ_FIRST(&scope);

      HashTableEntry_t *tmp = lookup_identifier($2->children[0]->children[0]->node_value.str_val, false);

      // populate code segment with the needed instructions
      char instr[512];
      $2->register_number = reserve_register();

      snprintf(instr, 510, "addI rsp, %d => rsp // function call\n",
               top_scope->base_register_offset);
      $2->code_part = add_instr_to_code_segment($2->code_part, instr);

      snprintf(instr, 510, "addI rpc, %d => r1 // function call\n",
               7 + $2->children[1]->num_children);
      $2->code_part = add_instr_to_code_segment($2->code_part, instr);

      snprintf(instr, 510, "storeAI r1 => rsp, 0 // function call\n");
      $2->code_part = add_instr_to_code_segment($2->code_part, instr);

      snprintf(instr, 510, "storeAI rsp => rsp, 4 // function call\n");
      $2->code_part = add_instr_to_code_segment($2->code_part, instr);

      snprintf(instr, 510, "storeAI rfp => rsp, 8 // function call\n");
      $2->code_part = add_instr_to_code_segment($2->code_part, instr);

      for (int i = 0; i < $2->children[1]->num_children; ++i) {
        snprintf(instr, 510, "storeAI r%d => rsp, %d // function call param\n",
                 $2->children[1]->children[i]->register_number, 12 + i * 4);
        $2->code_part = add_instr_to_code_segment($2->code_part, instr);
      }

      snprintf(instr, 510, "loadI 0 => r1 // function call\n");
      $2->code_part = add_instr_to_code_segment($2->code_part, instr);

      snprintf(instr, 510, "storeAI r1 => rsp, %d // function call\n",
               16 + $2->children[1]->num_children * 4);
      $2->code_part = add_instr_to_code_segment($2->code_part, instr);

      snprintf(instr, 510, "jumpI -> l%d // function call\n",
               tmp->start_label);
      $2->code_part = add_instr_to_code_segment($2->code_part, instr);

      snprintf(instr, 510, "loadAI rsp, %d => r%d // function call\n",
               12 + tmp->list_length * 4, $2->register_number);
      $2->code_part = add_instr_to_code_segment($2->code_part, instr);
    }
    else if ($2 != NULL && $2->node_type == AST_ATTRIBUTION
             && $2->num_children < 2) {
      TreeNode_t *node = create_node(AST_IDENTIFIER);
      add_child(node, create_literal_node($1));
      add_child_beginning($2, node);
    }
    else if ($2 != NULL && $2->num_children > 0) {
      if ($2->children[0] != NULL
          && $2->children[0]->node_type == AST_INDEXED_VECTOR)
      {
        TreeNode_t *node = create_node(AST_IDENTIFIER);
        add_child(node, create_literal_node($1));
        add_child_beginning($2->children[0], node);
      }
      else if ($2->children[0] != NULL
               && $2->children[0]->node_type == AST_ACCESSED_CLASS)
      {
        if ($2->children[0]->children[0] != NULL
          && $2->children[0]->children[0]->node_type == AST_INDEXED_VECTOR)
        {
          TreeNode_t *node = create_node(AST_IDENTIFIER);
          add_child(node, create_literal_node($1));
          add_child_beginning($2->children[0]->children[0], node);
        }
        else {
          TreeNode_t *node = create_node(AST_IDENTIFIER);
          add_child(node, create_literal_node($1));
          add_child_beginning($2->children[0], node);
        }
      }
      else {
        TreeNode_t *node = create_node(AST_IDENTIFIER);
        add_child(node, create_literal_node($1));
        add_child_beginning($2, node);
      }
    }
    else {
      TreeNode_t *node = create_node(AST_IDENTIFIER);
      add_child(node, create_literal_node($1));
      add_child_beginning($2, node);
    }
    $$ = $2;
  }
| TK_PR_STATIC opt_const simple_command3 {
    if ($3 != NULL) {
      add_child_beginning($3, $2);
      add_child_beginning($3, create_literal_node($1));
    }
    else {
      libera($2);
    }
    $$ = $3;
  }
| TK_PR_CONST simple_command3 {
    if ($2 != NULL)
     add_child_beginning($2, create_literal_node($1));
    $$ = $2;
  }
| TK_PR_IF '(' expression ')' TK_PR_THEN command_block simple_command5 {
    $$ = create_node(AST_IF_ELSE);

    add_child($$, $3);

    if (!can_coerce($3->infered_type, LT_BOOL)) {
      printf("Line %d: If condition must be a boolean expression.\n", yylineno);
      exit(ERR_WRONG_TYPE);
    }

    int first_label = reserve_label();
    int second_label = reserve_label();
    int third_label = reserve_label();

    char instr[512];

    snprintf(instr, 510, "cbr r%d -> l%d, l%d // if-else\n",
             $3->register_number, first_label, second_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    // if true block
    snprintf(instr, 510, "l%d: ", first_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
    add_child($$, $6);
    snprintf(instr, 510, "jumpI -> l%d // if-else\n", third_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
    snprintf(instr, 510, "l%d: ", second_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    // else block
    add_child($$, $7);
    snprintf(instr, 510, "jumpI -> l%d // if-else\n", third_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
    snprintf(instr, 510, "l%d: ", third_label);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
  }
;

simple_command2:
  // already read TK_IDENTIFICADOR
  // decide between class-type local variable declaration, attribution, shift commands and function call
  // if we read a TK_IDENTIFICADOR, then it,s a class-type local variable declaration
  // if we read an open bracket, or a dollar sign, or an equals sign, then it,s an attribution or a shift command
  // else, it,s a function call
  TK_IDENTIFICADOR {
    $$ = create_node(AST_LOCAL_DECLARATION);
    TreeNode_t *node = create_node(AST_IDENTIFIER);
    add_child(node, create_literal_node($1));
    add_child($$, node);
  }
| identifier_value2 simple_command4 {
    add_child_beginning($2, $1);
    $$ = $2;
}
| piped_function_call {
  $$ = $1;
}
;

simple_command3:
  // already read TK_PR_STATIC and/or TK_PR_CONST
  // decide between class-type and a primitive type variable declaration
  // if we read a primitive type, then it,s a primitive type variable declaration
  // if we read TK_IDENTIFICADOR, then it,s a class-type variable declaration
  TK_IDENTIFICADOR TK_IDENTIFICADOR {
    $$ = create_node(AST_LOCAL_DECLARATION);
    add_child($$, create_literal_node($1));

    TreeNode_t *node = create_node(AST_IDENTIFIER);
    add_child(node, create_literal_node($2));
    add_child($$, node);
  }
| primitive_type TK_IDENTIFICADOR opt_initialization {
    if ($3 != NULL) {
      $$ = create_node(AST_IDENTIFIER);
      add_child($$, create_literal_node($2));
      add_child_beginning($3, $$);

      add_child_beginning($3, create_literal_node($1));

      $$ = $3;
    }
    else {
      $$ = create_node(AST_LOCAL_DECLARATION);
      add_child($$, create_literal_node($1));

      TreeNode_t *node = create_node(AST_IDENTIFIER);
      add_child(node, create_literal_node($2));
      add_child($$, node);
    }
  }
;

simple_command4:
  // already read a primitive value
  // decide between attribution and shift commands
  // if we read an equals sign, then it,s and attribution
  // if we read a left shift or shift operator, then it,s a shift command
  '=' expression {
    $$ = create_node(AST_ATTRIBUTION);
    add_child($$, $2);
  }
| TK_OC_SL expression {
    $$ = create_node(AST_SHIFT);
    add_child($$, create_literal_node($1));
    add_child($$, $2);
  }
| TK_OC_SR expression {
    $$ = create_node(AST_SHIFT);
    add_child($$, create_literal_node($1));
    add_child($$, $2);
  }
;

simple_command5:
  // already read if ( expression ) then command_block
  // must decide between having or not an else statement
  %empty {
    // intentionally left NULL
    $$ = NULL;
  }
| TK_PR_ELSE command_block {
    $$ = $2;
  }
;


foreach_expr_list:
  expression foreach_expr_list_rest {
    add_child_beginning($2, $1);
    $$ = $2;
  }
;

foreach_expr_list_rest:
  %empty {
    $$ = create_node(AST_EXPRESSION_LIST);
  }
| foreach_expr_list_rest ',' expression {
    add_child($1, $3);
    $$ = $1;
}
;


for_command_list:
  command for_command_list_rest {
    add_child_beginning($2, $1);
    $$ = $2;
  }
;

for_command_list_rest:
  %empty {
    $$ = create_node(AST_FOR_COMMAND_LIST);
  }
| for_command_list_rest ',' command {
    add_child($1, $3);
    $$ = $1;
}
;


expression_list:
  expression expression_list_rest {
    add_child_beginning($2, $1);
    $$ = $2;
  }
;

expression_list_rest:
  %empty {
    $$ = create_node(AST_EXPRESSION_LIST);
  }
| expression_list_rest ',' expression {
    add_child($1, $3);
    $$ = $1;
  }
;

expression:
  expression1 '?' expression1 ':' expression {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);
    add_child($$, create_literal_node($4));
    add_child($$, $5);

    if (!can_coerce($1->infered_type, LT_BOOL)) {
      printf("Line %d: Ternary condition must be a boolean expression.\n", yylineno);
      exit(ERR_WRONG_TYPE);
    }

    $$->infered_type = type_infer($3->infered_type, $5->infered_type);
  }
| expression1 {
    $$ = $1;
  }
;

expression1:
  expression1 TK_OC_OR expression2 {
    $$ = create_node(AST_EXPRESSION);

    char instr[512];
    $$->register_number = reserve_register();
    int first_label = reserve_label();
    int second_label = reserve_label();
    int third_label = reserve_label();

    add_child($$, $1);

    snprintf(instr, 510, "cbr r%d -> l%d, l%d // expression\n",
             $1->register_number, first_label, second_label);

    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    add_child($$, create_literal_node($2));

    snprintf(instr, 510, "l%d: loadI 1 => r%d // expression\n", first_label, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "jumpI -> l%d // expression\n", third_label); 
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "l%d: // expression", second_label);

    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    add_child($$, $3);

    snprintf(instr, 510, "i2i r%d => r%d // expression\n", $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);


    $$->infered_type = type_infer($1->infered_type, $3->infered_type);
    snprintf(instr, 510, "l%d: // expression", third_label);

    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
  }
| expression2 {
    $$ = $1;
  }
;

expression2:
  expression2 TK_OC_AND expression3 {
    $$ = create_node(AST_EXPRESSION);
    char instr[512];
    $$->register_number = reserve_register();
    int first_label = reserve_label();
    int second_label = reserve_label();
    int third_label = reserve_label();

    add_child($$, $1);

    snprintf(instr, 510, "cbr r%d -> l%d, l%d // expression\n",
             $1->register_number, first_label, second_label);

    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    add_child($$, create_literal_node($2));

    snprintf(instr, 510, "l%d: loadI 0 => r%d // expression\n", second_label, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "jumpI -> l%d // expression\n", third_label); 
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

    snprintf(instr, 510, "l%d: // expression", first_label);

    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
    add_child($$, $3);
    
    snprintf(instr, 510, "i2i r%d => r%d // expression\n", $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);


    $$->infered_type = type_infer($1->infered_type, $3->infered_type);
    snprintf(instr, 510, "l%d: // expression", third_label);

    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
  }
| expression3 {
    $$ = $1;
  }
;

expression3:
  expression3 '|' expression4 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);
  }
| expression4 {
    $$ = $1;
  }
;

expression4:
  expression4 '^' expression5 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);
  }
| expression5 {
    $$ = $1;
  }
;

expression5:
  expression5 '&' expression6 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);
  }
| expression6 {
    $$ = $1;
  }
;

expression6:
  expression6 TK_OC_EQ expression7 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);

    char instr[512];
    $$->register_number = reserve_register();

    snprintf(instr, 510, "cmp_EQ r%d, r%d -> r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

  }
| expression6 TK_OC_NE expression7 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);

    char instr[512];
    $$->register_number = reserve_register();

    snprintf(instr, 510, "cmp_NE r%d, r%d -> r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

  }
| expression7 {
    $$ = $1;
  }
;

expression7:
  expression7 '<' expression8 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);

    //populate code segment with the needed instructions
    char instr[512];
    $$->register_number = reserve_register();

    snprintf(instr, 510, "cmp_LT r%d, r%d -> r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

  }
| expression7 '>' expression8 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);

    char instr[512];
    $$->register_number = reserve_register();

    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
    snprintf(instr, 510, "cmp_GT r%d, r%d -> r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

  }
| expression7 TK_OC_LE expression8 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);

    char instr[512];
    $$->register_number = reserve_register();

    snprintf(instr, 510, "cmp_LE r%d, r%d -> r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

  }
| expression7 TK_OC_GE expression8 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);
    char instr[512];
    $$->register_number = reserve_register();

    snprintf(instr, 510, "cmp_GE r%d, r%d -> r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);

  }
| expression8 {
    $$ = $1;
  }
;

expression8:
  expression8 '+' expression9 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);

    // populate code segment with the needed instructions
    char instr[512];
    $$->register_number = reserve_register();

    snprintf(instr, 510, "add r%d, r%d => r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
  }
| expression8 '-' expression9 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);

    // populate code segment with the needed instructions
    char instr[512];
    $$->register_number = reserve_register();

    snprintf(instr, 510, "sub r%d, r%d => r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
  }
| expression9 {
    $$ = $1;
  }
;

expression9:
  expression9 '*' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);

    // populate code segment with the needed instructions
    char instr[512];
    $$->register_number = reserve_register();

    snprintf(instr, 510, "mult r%d, r%d => r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
  }
| expression9 '/' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);

    // populate code segment with the needed instructions
    char instr[512];
    $$->register_number = reserve_register();

    snprintf(instr, 510, "div r%d, r%d => r%d // expression\n",
             $1->register_number, $3->register_number, $$->register_number);
    $$->code_part = add_instr_to_code_segment($$->code_part, instr);
  }
| expression9 '%' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, $1);
    add_child($$, create_literal_node($2));
    add_child($$, $3);

    $$->infered_type = type_infer($1->infered_type, $3->infered_type);
  }
| expression10 {
    $$ = $1;
  }
;

expression10:
  '+' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, create_literal_node($1));
    add_child($$, $2);

    $$->infered_type = $2->infered_type;
  }
| '-' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, create_literal_node($1));
    add_child($$, $2);

    $$->infered_type = $2->infered_type;
  }
| '!' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, create_literal_node($1));
    add_child($$, $2);

    $$->infered_type = $2->infered_type;
  }
| '&' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, create_literal_node($1));
    add_child($$, $2);

    $$->infered_type = $2->infered_type;
  }
| '*' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, create_literal_node($1));
    add_child($$, $2);

    $$->infered_type = $2->infered_type;
  }
| '?' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, create_literal_node($1));
    add_child($$, $2);

    $$->infered_type = $2->infered_type;
  }
| '#' expression10 {
    $$ = create_node(AST_EXPRESSION);
    add_child($$, create_literal_node($1));
    add_child($$, $2);

    $$->infered_type = $2->infered_type;
  }
| expression11 {
    $$ = $1;
  }
;

expression11:
  '(' expression ')' {
    $$ = $2;
  }
| expression_value {
    $$ = $1;

    if ($$->node_type == AST_LITERAL) {
      $$->infered_type = $$->node_value.literal_type;

      // populate code segment with the needed instructions
      char instr[512];
      $$->register_number = reserve_register();

      snprintf(instr, 510, "loadI %d => r%d // expression\n",
               $$->node_value.int_val, $$->register_number);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);
    }
    else if ($$->node_type == AST_IDENTIFIER) {
      HashTableEntry_t *tmp = lookup_identifier($$->children[0]->node_value.str_val, false);

      if (tmp->type_info.is_vector) {
        printf("Line %d: Missing index for vector \"%s\".\n", yylineno, tmp->id);
        exit(ERR_VECTOR);
      }

      if (tmp->type_info.nature == N_FUNC_DECL) {
        printf("Line %d: \"%s\" is a function.\n", yylineno, tmp->id);
        exit(ERR_FUNCTION);
      }

      if (tmp->type_info.nature == N_CLASS_DECL) {
        printf("Line %d: \"%s\" is a class.\n", yylineno, tmp->id);
        exit(ERR_USER);
      }

      $$->infered_type = tmp->type_info.literal_type;

      // populate code segment with the needed instructions
      char instr[512];
      $$->register_number = reserve_register();

      if (tmp->is_global) {
        snprintf(instr, 510, "loadAI rbss, %d => r%d // expression\n",
                 tmp->offset_from_base_reg, $$->register_number);
        $$->code_part = add_instr_to_code_segment($$->code_part, instr);
      }
      else {
        snprintf(instr, 510, "loadAI rfp, %d => r%d // expression\n",
                 tmp->offset_from_base_reg, $$->register_number);
        $$->code_part = add_instr_to_code_segment($$->code_part, instr);
      }
    }
    else if ($$->node_type == AST_PIPE_COMMANDS) {
      check_function_args($$, true);

      // get the topmost scope
      ScopeEntry_t *top_scope = TAILQ_FIRST(&scope);

      HashTableEntry_t *tmp = lookup_identifier($$->children[0]->children[0]->node_value.str_val, false);

      // populate code segment with the needed instructions
      char instr[512];
      $$->register_number = reserve_register();

      snprintf(instr, 510, "addI rsp, %d => rsp // function call\n",
               top_scope->base_register_offset);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      // only when calling from an expression
      // store all the registers in use
      for (int i = 0; i <= current_register_number; ++i) {
        snprintf(instr, 510, "storeAI r%d => rsp, %d // function call\n",
                 i, i * 4);
        $$->code_part = add_instr_to_code_segment($$->code_part, instr);
      }

      snprintf(instr, 510, "addI rsp, %d => rsp // function call\n",
               current_register_number * 4);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      snprintf(instr, 510, "addI rpc, %d => r1 // function call\n",
               7 + $$->children[1]->num_children);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      snprintf(instr, 510, "storeAI r1 => rsp, 0 // function call\n");
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      snprintf(instr, 510, "storeAI rsp => rsp, 4 // function call\n");
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      snprintf(instr, 510, "storeAI rfp => rsp, 8 // function call\n");
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      for (int i = 0; i < $$->children[1]->num_children; ++i) {
        snprintf(instr, 510, "storeAI r%d => rsp, %d // function call param\n",
                 $$->children[1]->children[i]->register_number, 12 + i * 4);
        $$->code_part = add_instr_to_code_segment($$->code_part, instr);
      }

      snprintf(instr, 510, "loadI 0 => r1 // function call\n");
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      snprintf(instr, 510, "storeAI r1 => rsp, %d // function call\n",
               16 + $$->children[1]->num_children * 4);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      snprintf(instr, 510, "jumpI -> l%d // function call\n",
               tmp->start_label);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      snprintf(instr, 510, "loadAI rsp, %d => r%d // function call\n",
               12 + tmp->list_length * 4, $$->register_number);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      // restore all the registers, except for the returned one
      snprintf(instr, 510, "subI rsp, %d => rsp // function call\n",
               current_register_number * 4);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);

      for (int i = 0; i <= current_register_number; ++i) {
        if (i == $$->register_number)
          continue;

        snprintf(instr, 510, "loadAI rsp, %d => r%d // function call\n",
                 i * 4, i);
        $$->code_part = add_instr_to_code_segment($$->code_part, instr);
      }

      snprintf(instr, 510, "subI rsp, %d => rsp // function call\n",
               top_scope->base_register_offset);
      $$->code_part = add_instr_to_code_segment($$->code_part, instr);
    }
    else if ($$->node_type == AST_INDEXED_VECTOR) {
      HashTableEntry_t *tmp = lookup_identifier($$->children[0]->children[0]->node_value.str_val, false);

      if (tmp->type_info.nature == N_FUNC_DECL) {
        printf("Line %d: \"%s\" is a function.\n", yylineno, tmp->id);
        exit(ERR_FUNCTION);
      }

      if (tmp->type_info.nature == N_CLASS_DECL) {
        printf("Line %d: \"%s\" is a class.\n", yylineno, tmp->id);
        exit(ERR_USER);
      }

      if (!tmp->type_info.is_vector) {
        printf("Line %d: Indexed variable \"%s\" is NOT a vector.\n", yylineno, tmp->id);
        exit(ERR_VARIABLE);
      }

      $$->infered_type = tmp->type_info.literal_type;
    }
    else if ($$->node_type == AST_ACCESSED_CLASS) {
      HashTableEntry_t *class_entry;

      if ($$->children[0] != NULL
          && $$->children[0]->node_type == AST_INDEXED_VECTOR)
      {
        class_entry = lookup_identifier($$->children[0]->children[0]->children[0]->node_value.str_val, false);
      }
      else {
        class_entry = lookup_identifier($$->children[0]->children[0]->node_value.str_val, false);
      }

      if (class_entry->type_info.nature == N_FUNC_DECL) {
        printf("Line %d: \"%s\" is a function.\n", yylineno, class_entry->id);
        exit(ERR_FUNCTION);
      }

      if (class_entry->type_info.literal_type != LT_USER) {
        printf("Line %d: Accessed variable \"%s\" is NOT a class.\n", yylineno, class_entry->id);
        exit(ERR_VARIABLE);
      }

      ClassAttributes_t *class_attr_entry;

      if ($$->children[0] != NULL
          && $$->children[0]->node_type == AST_INDEXED_VECTOR)
      {
        class_attr_entry = lookup_class_attr($$->children[0]->children[0]->children[0]->node_value.str_val, $$->children[1]->children[0]->node_value.str_val);
      }
      else {
        class_attr_entry = lookup_class_attr($$->children[0]->children[0]->node_value.str_val, $$->children[1]->children[0]->node_value.str_val);
      }

      $$->infered_type = class_attr_entry->type.literal_type;
    }
  }
;

expression_value:
  // at start of a value of an expression
  // alternatives:
  //  1) function call
  //  2) primitive value
  // if we read TK_IDENTIFICADOR, we must decide between function call and primitive value
  TK_IDENTIFICADOR expression_value2 {
    // this will be almost as messy as the one for simple_command
    // we must decide where to include this identificador, and it may
    // be a piped command or an identifier
    if ($2 != NULL
        && $2->node_type == AST_PIPE_COMMANDS)
    {
      TreeNode_t *node = create_node(AST_IDENTIFIER);
      add_child(node, create_literal_node($1));

      add_child_beginning($2, node);
      $$ = $2;

      check_function_args($2, false);
    }
    else if ($2 != NULL
            && $2->node_type == AST_INDEXED_VECTOR)
    {
      TreeNode_t *node = create_node(AST_IDENTIFIER);
      add_child(node, create_literal_node($1));

      add_child_beginning($2, node);
      $$ = $2;
    }
    else if ($2 != NULL
            && $2->node_type == AST_ACCESSED_CLASS)
    {
      if ($2->children[0] != NULL
          && $2->children[0]->node_type == AST_INDEXED_VECTOR)
      {
        TreeNode_t *node = create_node(AST_IDENTIFIER);
        add_child(node, create_literal_node($1));

        add_child_beginning($2->children[0], node);
        $$ = $2;
      }
      else {
        TreeNode_t *node = create_node(AST_IDENTIFIER);
        add_child(node, create_literal_node($1));

        add_child_beginning($2, node);
        $$ = $2;
      }
    }
    else {
      $$ = create_node(AST_IDENTIFIER);
      add_child($$, create_literal_node($1));
    }
  }
| numeric_value {
    $$ = $1;
  }
| boolean_value {
    $$ = $1;
  }
| TK_LIT_CHAR {
    $$ = create_literal_node($1);
  }
| TK_LIT_STRING {
    $$ = create_literal_node($1);
  }
;

expression_value2:
  // already read TK_IDENTIFICADOR
  // must decide between function call and primitive value
  // if we read a numeric value, a boolean value, a char or a string, then it,s a primitive value
  // if we read nothing, a dollar sign, or an open bracket, then it,s a primitive value
  // if we read an open parenthesis, then it,s a function call
  identifier_value2 {
    $$ = $1;
  }
| piped_function_call {
    $$ = $1;
  }
;


piped_function_call:
  function_call piped_function_calls_list {
    add_child_beginning($2, $1);
    $$ = $2;
  }
;

function_call:
  '(' function_call_param_list ')' {
    $$ = $2;
  }
| '(' ')' {
    $$ = create_node(AST_EXPRESSION_LIST);
  }
;

function_call_param_list:
  expression function_call_param_list_rest {
    add_child_beginning($2, $1);
    $$ = $2;
  }
| '.' function_call_param_list_rest {
    add_child_beginning($2, create_node(AST_PIPE_DOT));
    $$ = $2;
  }
;

function_call_param_list_rest:
  %empty {
    $$ = create_node(AST_EXPRESSION_LIST);
  }
| function_call_param_list_rest ',' expression {
    add_child($1, $3);
    $$ = $1;
  }
| function_call_param_list_rest ',' '.' {
    add_child($1, create_node(AST_PIPE_DOT));
    $$ = $1;
  }
;

piped_function_calls_list:
  %empty {
    $$ = create_node(AST_PIPE_COMMANDS);
  }
| piped_function_calls_list TK_OC_FORWARD_PIPE TK_IDENTIFICADOR function_call {
    add_child($1, create_literal_node($2));

    $$ = create_node(AST_IDENTIFIER);
    add_child($$, create_literal_node($3));
    add_child($1, $$);

    add_child($1, $4);
    $$ = $1;
  }
| piped_function_calls_list TK_OC_BASH_PIPE TK_IDENTIFICADOR function_call {
    add_child($1, create_literal_node($2));

    $$ = create_node(AST_IDENTIFIER);
    add_child($$, create_literal_node($3));
    add_child($1, $$);

    add_child($1, $4);
    $$ = $1;
  }
;


opt_static:
  %empty {
    // intentionally left NULL
    $$ = NULL;
  }
| TK_PR_STATIC {
    $$ = create_literal_node($1);
  }
;

opt_const:
  %empty {
    // intentionally left NULL
    $$ = NULL;
  }
| TK_PR_CONST {
    $$ = create_literal_node($1);
  }
;

opt_encapsulation_type:
  %empty {
    $$ = NULL;
  }
| TK_PR_PUBLIC {
  $$ = create_literal_node($1);
}
| TK_PR_PROTECTED {
  $$ = create_literal_node($1);
}
| TK_PR_PRIVATE {
  $$ = create_literal_node($1);
}
;

primitive_type:
  TK_PR_INT {
    $$ = $1;
  }
| TK_PR_FLOAT {
    $$ = $1;
  }
| TK_PR_BOOL {
    $$ = $1;
  }
| TK_PR_CHAR {
    $$ = $1;
  }
| TK_PR_STRING {
    $$ = $1;
  }
;

any_type:
  primitive_type {
    $$ = $1;
  }
| TK_IDENTIFICADOR {
    $$ = $1;
  }
;

opt_class_field:
  %empty {
    $$ = NULL;
  }
| '$' TK_IDENTIFICADOR {
    $$ = create_node(AST_ACCESSED_CLASS);

    TreeNode_t *node = create_node(AST_IDENTIFIER);
    add_child(node, create_literal_node($2));

    add_child($$, node);
  }
;

opt_initialization:
  %empty {
    // intentionally left NULL
    $$ = NULL;
  }
| TK_OC_LE primitive_value {
    $$ = create_node(AST_INITIALIZATION);
    add_child($$, $2);
  }
;

primitive_value:
  identifier_value
| numeric_value
| boolean_value
| TK_LIT_CHAR {
    $$ = create_literal_node($1);
  }
| TK_LIT_STRING {
    $$ = create_literal_node($1);
  }
;

numeric_value:
  TK_LIT_INT {
    $$ = create_literal_node($1);
  }
| TK_LIT_FLOAT {
    $$ = create_literal_node($1);
  }
;

boolean_value:
  TK_LIT_TRUE {
    $$ = create_literal_node($1);
  }
| TK_LIT_FALSE {
    $$ = create_literal_node($1);
  }
;

identifier_value:
  // decide between class field identifier and array item identifier
  // alternatives:
  //  1) identifier
  //  2) class field identifier
  //  3) array item identifier
  TK_IDENTIFICADOR identifier_value2 {
    if ($2 != NULL) {
      add_child_beginning($2, create_literal_node($1));
      $$ = $2;
    }
    else {
      $$ = create_node(AST_IDENTIFIER);
      add_child($$, create_literal_node($1));
    }
  }
;

identifier_value2:
  // already read TK_IDENTIFICADOR
  // must decide between class field identifier and array item identifier
  // if we read nothing else, then it,s an identifier
  // if we read a dollar sign, then it,s a class field identifier
  // if we read an open bracket, then it,s an array item identifier
  %empty {
    // intentionally left NULL
    $$ = NULL;
  }
| '$' TK_IDENTIFICADOR {
    $$ = create_node(AST_ACCESSED_CLASS);

    TreeNode_t *node = create_node(AST_IDENTIFIER);
    add_child(node, create_literal_node($2));

    add_child_beginning($$, node);
  }
| '[' expression ']' opt_class_field {
    $$ = create_node(AST_INDEXED_VECTOR);
    add_child($$, $2);

    if ($4 != NULL) {
      add_child_beginning($4, $$);
      $$ = $4;
    }

    if (!can_coerce($2->infered_type, LT_INT)) {
      printf("Line %d: Vector index must be an arithmetic expression.\n", yylineno);
      exit(ERR_WRONG_TYPE);
    }
  }
;

%%

void yyerror(char const *s) {
  printf("Line %d: %s near \"%s\"\n",
         yylineno, s, yylval.valor_lexico.str_val);
}

void descompila(void* tree) {
  // DEBUG: uncomment for testing
  // return;

  _descompila(tree, NULL);
}

void compila(void* tree) {
  // DEBUG: uncomment for testing
  // return;

  TreeNode_t *tmp_tree = tree;
  dump_code(tmp_tree->code_part, 0);

  // dump_tree(tree, 0);
}

void _descompila(TreeNode_t *node, const char* separator) {
  if (node == NULL)
    return;
  
  int last_child = node->num_children - 1;

  switch (node->node_type) {
    case AST_PROGRAM:
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], "");
      }
      break;
    case AST_FUNCTION:
      for (int i = 0; i < last_child; ++i) {
        if (node->children[i]->node_type == AST_PARAMETERS_LIST) {
            printf(" (");
            _descompila(node->children[i], ",");  // static? type name
            printf("){");
        } else {
          _descompila(node->children[i], "");  // static? type name
          printf(" ");
        }
      }
      for (int i = 0; i < node->children[last_child]->num_children; ++i)
        _descompila(node->children[last_child]->children[i], ";");  // prints command list
      printf("}\n");
      break;
    case AST_CLASS:
      printf("\nclass ");
      _descompila(node->children[0], "");
      printf("[\n");
      for (int i = 1; i < node->num_children; ++i) {
        _descompila(node->children[i], "");
          if (i != last_child)
            printf(" : \n");
      }
      printf("];\n");
      break;
    case AST_CLASS_ATTRIBUTE:
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], "");
        printf(" ");
      }
      break;
    case AST_PARAMETERS_LIST:
      for (int i = 0; i < node->num_children; ++i) {
        if (i != node->num_children - 1)
          _descompila(node->children[i], ",");  // prints parameters
        else
          _descompila(node->children[i], "");  // prints last parameter
      }
      break;
    case AST_PARAMETER:
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], "");  // prints parameters
        printf(" ");
      }
      printf("%s ", separator);
      break;
    case AST_PIPE_COMMANDS:
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], "");
        printf("(");
        if (i + 1 < node->num_children) {
          i += 1;
          _descompila(node->children[i], "");
        }
        printf(")");
        if (i + 1 < node->num_children) {
          i += 1;
          _descompila(node->children[i], "");
        }
      }
      break;
    case AST_IDENTIFIER:
      _descompila(node->children[0], "");
      break;
    case AST_LITERAL:
      if (node->node_value.token_type == TT_LITERAL) {
        switch (node->node_value.literal_type) {
          case LT_CHAR:
            printf("'%s'", node->node_value.str_val);
            break;
          case LT_STRING:
            printf("\"%s\"", node->node_value.str_val);
            break;
          default:
            printf("%s", node->node_value.str_val);
            break;
        }
      }
      else {
        printf("%s", node->node_value.str_val);
      }
      break;
    case AST_COMMAND_LIST:
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], "");
        if (node->children[i]->node_type != AST_CASE)
          printf("%s\n", separator);
      }
      break;
    case AST_FOR_COMMAND_LIST:
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], "");
        if (i != node->num_children - 1)
          printf("%s ", separator);
      }
      break;
    case AST_EXPRESSION_LIST:
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], "");
        if (i != node->num_children - 1)
          printf(", ");
      }
      break;
    case AST_BLOCK:
      printf("{\n");
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], ";");
      }
      printf("}\n");
      break;
    case AST_BREAK:
      printf("\nbreak");
      break;
    case AST_CONTINUE:
      printf("\ncontinue");
      break;
    case AST_INPUT:
      printf("\ninput ");
      _descompila(node->children[0], ""); // prints expression
      break;
    case AST_OUTPUT:
      printf("\noutput ");
      _descompila(node->children[0], ";"); // prints expression list
      break;
    case AST_RETURN:
      printf("\nreturn ");
      _descompila(node->children[0], ";"); // prints expression
      break;
    case AST_EXPRESSION:
      if (node->num_children > 1)
        printf("(");
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], "");
      }
      if (node->num_children > 1)
        printf(")");
      break;
    case AST_SWITCH:
      printf("switch (");
      _descompila(node->children[0], "");
      printf(")");
      _descompila(node->children[1], ";");
      printf("\n");
      break;
    case AST_CASE:
      printf("\ncase ");
      _descompila(node->children[0], "");
      printf(":\n");
      break;
    case AST_WHILE:
      printf("\nwhile (");
      _descompila(node->children[0], ""); // print expression
      printf(") do");
      _descompila(node->children[1], separator); // print command block
      printf("\n");
      break;
    case AST_DO_WHILE:
      printf("\ndo");
      _descompila(node->children[0], ""); // print command block
      printf(" while (");
      _descompila(node->children[1], separator); // print expression
      printf(")");
      break;
    case AST_FOREACH:
      printf("\nforeach (");
      _descompila(node->children[0], "");
      printf(": ");
      _descompila(node->children[1], "");
      printf(")");
      _descompila(node->children[2], separator);
      printf("\n");
      break;
    case AST_FOR:
      printf("\nfor (");
      _descompila(node->children[0], ",");
      printf(": ");
      _descompila(node->children[1], ",");
      printf(": ");
      _descompila(node->children[2], ",");
      printf(")");
      _descompila(node->children[3], separator);
      printf("\n");
      break;
    case AST_ATTRIBUTION:
      printf("\n");
      _descompila(node->children[0], "");
      printf(" = ");
      _descompila(node->children[1], separator);
      break;
    case AST_INITIALIZATION:
      printf("\n");
      for (int i = 0; i < node->num_children; i ++) {
        if (i == last_child) {
          printf("<= ");
        }
        _descompila(node->children[i], "");
        printf(" ");
      }
      break;
    case AST_GLOBAL_DECLARATION:
      printf("\n");
      for (int i = 0; i < node->num_children; i ++) {
        if (node->children[i]->node_type == AST_INDEXED_VECTOR) {
          _descompila(node->children[i]->children[0], "");
          printf("[");
          _descompila(node->children[i]->children[1], "");
          printf("]");
        } else {
          _descompila(node->children[i], "");
        }
        printf(" ");
      }
      printf(";");
      break;
    case AST_LOCAL_DECLARATION:
      printf("\n");
      for (int i = 0; i < node->num_children; i ++) {
        _descompila(node->children[i], "");
        printf(" ");
      }
      break;
    case AST_SHIFT:
      printf("\n");
      for (int i = 0; i < node->num_children; ++i) {
        _descompila(node->children[i], separator);
      }
      break;
    case AST_IF_ELSE:
      printf("\nif (");
      _descompila(node->children[0], "");
      printf(") then");
      _descompila(node->children[1], separator);
      printf("\n");

      if (node->num_children == 3) {
        printf("\nelse");
        _descompila(node->children[2], separator);
        printf("\n");
      }
      break;
    case AST_ACCESSED_CLASS:
      _descompila(node->children[0], "");
      for (int i =1; i < node->num_children; i++) {
        if (node->children[i]->node_type == AST_IDENTIFIER)
          printf("$");
        _descompila(node->children[i], "");
      }
      break;
    case AST_INDEXED_VECTOR:
      _descompila(node->children[0], "");
      for (int i =1; i < node->num_children; i++) {
        printf("[");
        _descompila(node->children[i], "");
        printf("]");
      }
      break;
    case AST_PIPE_DOT:
      printf(".");
      break;
    default:
      printf("NOT IMPLEMENTED YET: %d\n", node->node_type);
  }
}

void libera(void* tree) {
  if (tree == NULL)
    return;

  TreeNode_t *node = (TreeNode_t *) tree;

  // free children recursively
  for (int i = 0; i < node->num_children; ++i)
    libera(node->children[i]);

  // free vector of children pointers
  free(node->children);
  node->children = NULL;

  // free code
  if (node->code_part != NULL) {
    struct code_s *tmp_code = node->code_part;
    while (!TAILQ_EMPTY(tmp_code)) {
      CodePart_t *code_part = TAILQ_FIRST(tmp_code);
      TAILQ_REMOVE(tmp_code, code_part, codes);
      free(code_part);
    }

    free(node->code_part);
    node->code_part = NULL;
  }

  // free the node itself
  free(node);
  node = NULL;

  // free the scopes
  while (!TAILQ_EMPTY(&scope))
    pop_scope();
}

void print_indent(int indent) {
  for (int i = 0; i < indent; ++i)
    printf("  ");
}

void dump_tree(TreeNode_t *node, int indent) {
  if (node == NULL)
    return;

  switch (node->node_type) {
    case AST_PROGRAM:
      print_indent(indent);
      printf("node_type: AST_PROGRAM\n");
      break;
    case AST_IDENTIFIER:
      print_indent(indent);
      printf("node_type: AST_IDENTIFIER\n");
      break;
    case AST_LITERAL:
      print_indent(indent);
      printf("node_type: AST_LITERAL\n");
      switch (node->node_value.literal_type) {
        case LT_INT:
          print_indent(indent);
          printf("(int) %d\n", node->node_value.int_val);
          break;
        case LT_FLOAT:
          print_indent(indent);
          printf("(float) %f\n", node->node_value.float_val);
          break;
        case LT_STRING:
          print_indent(indent);
          printf("(string) \"%s\"\n", node->node_value.str_val);
          break;
        case LT_CHAR:
          print_indent(indent);
          printf("(char) '%c'\n", node->node_value.char_val);
          break;
        case LT_BOOL:
          print_indent(indent);
          printf(node->node_value.bool_val ? "(bool) true\n" : "(bool) false\n");
          break;
        default:
          break;
      }
      break;
    case AST_FUNCTION:
      print_indent(indent);
      printf("node_type: AST_FUNCTION\n");
      break;
    case AST_IF_ELSE:
      print_indent(indent);
      printf("node_type: AST_IF_ELSE\n");
      break;
    case AST_SWITCH:
      print_indent(indent);
      printf("node_type: AST_SWITCH\n");
      break;
    case AST_CASE:
      print_indent(indent);
      printf("node_type: AST_CASE\n");
      break;
    case AST_DO_WHILE:
      print_indent(indent);
      printf("node_type: AST_DO_WHILE\n");
      break;
    case AST_WHILE:
      print_indent(indent);
      printf("node_type: AST_WHILE\n");
      break;
    case AST_FOR:
      print_indent(indent);
      printf("node_type: AST_FOR\n");
      break;
    case AST_FOREACH:
      print_indent(indent);
      printf("node_type: AST_FOREACH\n");
      break;
    case AST_BREAK:
      print_indent(indent);
      printf("node_type: AST_BREAK\n");
      break;
    case AST_CONTINUE:
      print_indent(indent);
      printf("node_type: AST_CONTINUE\n");
      break;
    case AST_ATTRIBUTION:
      print_indent(indent);
      printf("node_type: AST_ATTRIBUTION\n");
      break;
    case AST_SHIFT:
      print_indent(indent);
      printf("node_type: AST_SHIFT\n");
      break;
    case AST_PARAMETERS_LIST:
      print_indent(indent);
      printf("node_type: AST_PARAMETERS_LIST\n");
      break;
    case AST_PARAMETER:
      print_indent(indent);
      printf("node_type: AST_PARAMETER\n");
      break;
    case AST_GLOBAL_DECLARATION:
      print_indent(indent);
      printf("node_type: AST_GLOBAL_DECLARATION\n");
      break;
    case AST_LOCAL_DECLARATION:
      print_indent(indent);
      printf("node_type: AST_LOCAL_DECLARATION\n");
      break;
    case AST_CLASS:
      print_indent(indent);
      printf("node_type: AST_CLASS\n");
      break;
    case AST_CLASS_ATTRIBUTE:
      print_indent(indent);
      printf("node_type: AST_CLASS_ATTRIBUTE\n");
      break;
    case AST_INITIALIZATION:
      print_indent(indent);
      printf("node_type: AST_INITIALIZATION\n");
      break;
    case AST_INPUT:
      print_indent(indent);
      printf("node_type: AST_INPUT\n");
      break;
    case AST_OUTPUT:
      print_indent(indent);
      printf("node_type: AST_OUTPUT\n");
      break;
    case AST_RETURN:
      print_indent(indent);
      printf("node_type: AST_RETURN\n");
      break;
    case AST_BLOCK:
      print_indent(indent);
      printf("node_type: AST_BLOCK\n");
      break;
    case AST_PIPE_COMMANDS:
      print_indent(indent);
      printf("node_type: AST_PIPE_COMMANDS\n");
      break;
    case AST_EXPRESSION:
      print_indent(indent);
      printf("node_type: AST_EXPRESSION\n");
      break;
    case AST_INDEXED_VECTOR:
      print_indent(indent);
      printf("node_type: AST_INDEXED_VECTOR\n");
      break;
    case AST_ACCESSED_CLASS:
      print_indent(indent);
      printf("node_type: AST_ACCESSED_CLASS\n");
      break;
    case AST_EXPRESSION_LIST:
      print_indent(indent);
      printf("node_type: AST_EXPRESSION_LIST\n");
      break;
    case AST_COMMAND_LIST:
      print_indent(indent);
      printf("node_type: AST_COMMAND_LIST\n");
      break;
    case AST_FOR_COMMAND_LIST:
      print_indent(indent);
      printf("node_type: AST_FOR_COMMAND_LIST\n");
      break;
    case AST_PIPE_DOT:
      print_indent(indent);
      printf("node_type: AST_PIPE_DOT\n");
      break;
    default:
      printf("NOT IMPLEMENTED YET: %d\n", node->node_type);
  }

  if (node->code_part != NULL
      && !TAILQ_EMPTY((struct code_s *) node->code_part))
  {
    print_indent(indent);
    printf("code_part:\n");
    dump_code(node->code_part, indent + 1);
  }

  if (node->register_number > 0) {
    print_indent(indent);
    printf("register_number: %d\n", node->register_number);
  }

  if (node->num_children > 0) {
    print_indent(indent);
    printf("num_children: %d\n", node->num_children);
    for (int i = 0; i < node->num_children; ++i) {
      dump_tree(node->children[i], indent + 1);
    }
  }
}

void dump_code(struct code_s *code, int indent) {
  if (code == NULL)
    return;

  CodePart_t *cur_instr;
  TAILQ_FOREACH(cur_instr, code, codes) {
    print_indent(indent);
    printf("%s", cur_instr->instruction);
  }
}

TreeNode_t *create_node(NodeType_t tipo) {
  TreeNode_t *node = calloc(1, sizeof(TreeNode_t));
  node->node_type = tipo;

  return node;
}

TreeNode_t *create_literal_node(LexicalValue_t valor) {
  TreeNode_t *node = create_node(AST_LITERAL);
  node->node_value = valor;
  snprintf(node->node_value.str_val, 510, "%s", valor.str_val);
 
  return node;
}

TreeNode_t *create_root() {
  TreeNode_t *program = create_node(AST_PROGRAM);
  struct code_s *code_part = (struct code_s *)program->code_part;
  code_part = calloc(1, sizeof(struct code_s));
  TAILQ_INIT(code_part);
  program->code_part = code_part;
  return program;
}

void add_child(TreeNode_t *node, TreeNode_t *child_node) {
  insert_child(node, child_node, false);
}

void add_child_beginning(TreeNode_t *node, TreeNode_t *child_node) {
  insert_child(node, child_node, true);
}

void insert_child(TreeNode_t *node, TreeNode_t *child_node, bool beginning) {
  if (node == NULL || child_node == NULL)
    return;

  node->num_children += 1;
  node->children = realloc(
      node->children,
      node->num_children * sizeof(TreeNode_t *)
  );

  // if we need to add to the left side because of some recursion
  if (beginning && node->num_children > 1) {
    // make room for a new first child
    memmove(&node->children[1],
            &node->children[0],
            (node->num_children - 1) * sizeof(TreeNode_t *)
    );

    // add to the start of the list
    node->children[0] = child_node;
  }
  else {
    node->children[node->num_children - 1] = child_node;
  }

  // also concat the codes
  if (!beginning)
    node->code_part = concat_code_segments(node->code_part, child_node->code_part);
  else
    node->code_part = concat_code_segments(child_node->code_part, node->code_part);

  child_node->code_part = NULL;
}

void set_static(HashTableEntry_t *entry, TreeNode_t *node) {
  // Global declaration without vector
  if (node->num_children > 1
      && node->children[1]->node_type == AST_LITERAL
      && strcmp("static", node->children[1]->node_value.str_val) == 0)
  {
    entry->type_info.is_static = true;
  }

  // Global declaration with vector
  if (node->num_children > 2
      && node->children[2]->node_type == AST_LITERAL
      && strcmp("static", node->children[2]->node_value.str_val) == 0)
  {
    entry->type_info.is_static = true;
  }

  // Local declaration
  if (node->num_children > 0
      && node->children[0]->node_type == AST_LITERAL
      && strcmp("static", node->children[0]->node_value.str_val) == 0)
  {
    entry->type_info.is_static = true;
  }
}

void set_vector(HashTableEntry_t *entry, TreeNode_t *node) {
  // Global declaration
  if (node->num_children > 0
      && node->children[0]->node_type == AST_INDEXED_VECTOR)
  {
    entry->type_info.is_vector = true;
  }
}


void set_entry_type_and_size(HashTableEntry_t *entry, LexicalValue_t literal_node, bool is_class_declaration, int vector_size) {
  if (is_class_declaration) {
    entry->type_info.nature = N_CLASS_DECL;
    entry->type_info.literal_type = LT_USER;
    entry->bytes_size = 0;
    for (int i = 0; i < entry->list_length; i ++) {
      entry->bytes_size += entry->attributes_list[i].bytes_size;
    }
    entry->bytes_size *= vector_size;
  }
  else if (strcmp("int", literal_node.str_val) == 0) {
    entry->type_info.nature = N_VARIABLE;
    entry->type_info.literal_type = LT_INT;
    entry->bytes_size = 4 * vector_size;
  }
  else if (strcmp("float", literal_node.str_val) == 0) {
    entry->type_info.nature = N_VARIABLE;
    entry->type_info.literal_type = LT_FLOAT;
    entry->bytes_size = 8 * vector_size;
  }
  else if (strcmp("char", literal_node.str_val) == 0) {
    entry->type_info.nature = N_VARIABLE;
    entry->type_info.literal_type = LT_CHAR;
    entry->bytes_size = 1 * vector_size;
  }
  else if (strcmp("string", literal_node.str_val) == 0) {
      entry->type_info.nature = N_VARIABLE; 
    entry->type_info.literal_type = LT_STRING;
    entry->bytes_size = -1; // must be set on first attribution
  }
  else if (strcmp("bool", literal_node.str_val) == 0) {
      entry->type_info.nature = N_VARIABLE; 
    entry->type_info.literal_type = LT_BOOL;
    entry->bytes_size = 1 * vector_size;
  }
  else {
    // user type
    HashTableEntry_t *tmp = lookup_identifier(literal_node.str_val, true);
    // check if user type used is declared
    if (tmp == NULL || (tmp != NULL && tmp->type_info.nature != N_CLASS_DECL)) {
      printf("Line %d: User type \"%s\" not declared.\n", yylineno, literal_node.str_val);
      exit(ERR_UNDECLARED);
    }
    entry->type_info.literal_type = LT_USER;
    entry->type_info.nature = N_VARIABLE;
    snprintf(entry->type_info.class_name, 510, "%s", literal_node.str_val);
    entry->bytes_size = tmp->bytes_size * vector_size;
  }
}

void set_class_attr_type_and_size(ClassAttributes_t *class_attribute, LexicalValue_t literal_node) {
  if (strcmp("int", literal_node.str_val) == 0) {
    class_attribute->type.literal_type = LT_INT;
    class_attribute->bytes_size = 4;
  }
  else if (strcmp("float", literal_node.str_val) == 0) {
    class_attribute->type.literal_type = LT_FLOAT;
    class_attribute->bytes_size = 8;
  }
  else if (strcmp("char", literal_node.str_val) == 0) {
    class_attribute->type.literal_type = LT_CHAR;
    class_attribute->bytes_size = 1;
  }
  else if (strcmp("string", literal_node.str_val) == 0) {
    class_attribute->type.literal_type = LT_STRING;
    class_attribute->bytes_size = -1; // must be set on first attribution
  }
  else if (strcmp("bool", literal_node.str_val) == 0) {
    class_attribute->type.literal_type = LT_BOOL;
    class_attribute->bytes_size = 1;
  }
}

void set_func_args_type_and_size(FunctionArgs_t *function_args, LexicalValue_t literal_node) {
  if (strcmp("int", literal_node.str_val) == 0) {
    function_args->type.literal_type = LT_INT;
    function_args->bytes_size = 4;
  }
  else if (strcmp("float", literal_node.str_val) == 0) {
    function_args->type.literal_type = LT_FLOAT;
    function_args->bytes_size = 8;
  }
  else if (strcmp("char", literal_node.str_val) == 0) {
    function_args->type.literal_type = LT_CHAR;
    function_args->bytes_size = 1;
  }
  else if (strcmp("string", literal_node.str_val) == 0) {
    function_args->type.literal_type = LT_STRING;
    function_args->bytes_size = -1; // must be set on first attribution
  }
  else if (strcmp("bool", literal_node.str_val) == 0) {
    function_args->type.literal_type = LT_BOOL;
    function_args->bytes_size = 1;
  } else {
    function_args->type.literal_type = LT_USER;
    HashTableEntry_t *tmp = lookup_identifier(literal_node.str_val, true);

    if (tmp == NULL || tmp->type_info.nature != N_CLASS_DECL) {
        printf("Line %d: User type \"%s\" not declared.\n", yylineno, literal_node.str_val);
        exit(ERR_UNDECLARED);
    }

    snprintf(function_args->type.class_name, 510, "%s", literal_node.str_val);
  }
  function_args->type.nature = N_VARIABLE;

}


bool can_coerce(LiteralType_t from, LiteralType_t to) {
  if (from == to)
    return true;

  if (to == LT_STRING || to == LT_CHAR || to == LT_USER)
    return false;

  if (from == LT_INT && to == LT_FLOAT
      || from == LT_INT && to == LT_BOOL
      || from == LT_BOOL && to == LT_FLOAT
      || from == LT_BOOL && to == LT_INT
      || from == LT_FLOAT && to == LT_INT
      || from == LT_FLOAT && to == LT_BOOL)
  {
    return true;
  }

  return false;
}

LiteralType_t type_infer(LiteralType_t t1, LiteralType_t t2) {
  // both ints, floats or bools
  if ((t1 == LT_INT && t2 == LT_INT)
      || (t1 == LT_FLOAT && t2 == LT_FLOAT)
      || (t1 == LT_BOOL && t2 == LT_BOOL))
  {
    return t1;
  }

  // a float and an int
  if ((t1 == LT_FLOAT || t2 == LT_FLOAT)
      && (t1 == LT_INT || t2 == LT_INT))
  {
    return LT_FLOAT;
  }

  // a bool and an int
  if ((t1 == LT_BOOL || t2 == LT_BOOL)
      && (t1 == LT_INT || t2 == LT_INT))
  {
    return LT_INT;
  }

  // a float and a bool
  if ((t1 == LT_FLOAT || t2 == LT_FLOAT)
      && (t1 == LT_BOOL || t2 == LT_BOOL))
  {
    return LT_FLOAT;
  }

  printf("Line %d: Incompatible types in expression.\n", yylineno);
  exit(ERR_WRONG_TYPE);
}

void set_const(HashTableEntry_t *entry, TreeNode_t *node) {
  if (strcmp("const", node->children[0]->node_value.str_val) == 0) {
    entry->type_info.is_const = true;
  }
  else if (strcmp("const", node->children[1]->node_value.str_val) == 0) {
    entry->type_info.is_const = true;
  }
}

void set_encapsulation(ClassAttributes_t *class_attribute, LexicalValue_t literal_node) {
    if (strcmp("public", literal_node.str_val) == 0) {
      class_attribute->encapsulation = ENC_PUBLIC;
    }
    else if (strcmp("protected", literal_node.str_val) == 0) {
      class_attribute->encapsulation = ENC_PROTECTED;
    }
    else if (strcmp("private", literal_node.str_val) == 0) {
      class_attribute->encapsulation = ENC_PRIVATE;
    }
    else {
      class_attribute->encapsulation = ENC_PUBLIC;
    }
}

void set_offset_from_base_reg(HashTableEntry_t *entry) {
  // if we're in global scope
  if (TAILQ_NEXT(TAILQ_FIRST(&scope), scopes) == NULL) {
    entry->offset_from_base_reg = reserve_global_memory(entry->bytes_size);
    entry->is_global = true;
  }
  else {
    entry->offset_from_base_reg = reserve_local_memory(entry->bytes_size);
    entry->is_global = false;
  }
}

HashTableEntry_t *lookup_identifier(char *id, bool allow_return_null) {
  ScopeEntry_t *cur_scope;
  TAILQ_FOREACH(cur_scope, &scope, scopes) {
    HashTableEntry_t *tmp;

    HASH_FIND_STR(cur_scope->hash_table, id, tmp);

    if (tmp != NULL)
      return tmp;
  }

  if (!allow_return_null) {
    printf("Line %d: Undeclared identifier \"%s\".\n", yylineno, id);
    exit(ERR_UNDECLARED);
  }

  return NULL;
}

HashTableEntry_t *lookup_identifier_current_scope(char *id, bool allow_return_null) {
  ScopeEntry_t *top_scope = TAILQ_FIRST(&scope);
  HashTableEntry_t *tmp;

  HASH_FIND_STR(top_scope->hash_table, id, tmp);

  if (tmp != NULL)
    return tmp;

  if (!allow_return_null) {
    printf("Line %d: Undeclared identifier \"%s\".\n", yylineno, id);
    exit(ERR_UNDECLARED);
  }

  return NULL;
}


ClassAttributes_t *lookup_class_attr(char *class_type, char *class_attr) {
  ScopeEntry_t *cur_scope;
  char class_declaration[510] = "";

  TAILQ_FOREACH(cur_scope, &scope, scopes) {
    HashTableEntry_t *tmp;

    HASH_FIND_STR(cur_scope->hash_table, class_type, tmp);
    if (tmp!= NULL && tmp->type_info.literal_type == LT_USER) {
      snprintf(class_declaration, 510, "%s", tmp->type_info.class_name);
    }

    if (strlen(class_declaration) > 0) {
      HASH_FIND_STR(cur_scope->hash_table, class_declaration, tmp);
      if (tmp != NULL && tmp->type_info.nature == N_CLASS_DECL) {
        for (int i = 0; i < tmp->list_length; i++) {
          if (strcmp(tmp->attributes_list[i].id, class_attr) == 0) {
            return &tmp->attributes_list[i];
          }
        }
      }
    }
  }

  printf("Line %d: Class \"%s\" doesn't have attribute \"%s\".\n", yylineno, class_type, class_attr);
  exit(ERR_USER);

  return NULL;
}

void check_function_args(TreeNode_t* node, bool has_infered_type) {
  for (int i = 0; i < node->num_children; i+=3) {
    // steps of three:
    // i => function name
    // i + 1 => expression list
    // i + 2 => if it exists, pipe command (never accesed)

    HashTableEntry_t *tmp = lookup_identifier(node->children[i]->children[0]->node_value.str_val, false);

    if (tmp->type_info.nature != N_FUNC_DECL) {
      printf("Line %d: \"%s\" is NOT a function.\n", yylineno, node->children[i]->children[0]->node_value.str_val);
      if (tmp->type_info.nature == N_VARIABLE && tmp->type_info.is_vector)
        exit(ERR_VECTOR);
      else if (tmp->type_info.nature == N_VARIABLE)
        exit(ERR_VARIABLE);
      else if (tmp->type_info.nature == N_CLASS_DECL)
        exit(ERR_USER);
      else
        exit(ERR_VARIABLE);
    }
    else if (tmp->list_length > node->children[i + 1]->num_children) {
      printf("Line %d: Function \"%s\" is missing arguments.\n", yylineno, node->children[i]->children[0]->node_value.str_val);
      exit(ERR_MISSING_ARGS);
    }
    else if (tmp->list_length < node->children[i + 1]->num_children) {
      printf("Line %d: Function \"%s\" has too many arguments.\n", yylineno, node->children[i]->children[0]->node_value.str_val);
      exit(ERR_EXCESS_ARGS);
    }
    else {
      for (int j=0; j < node->children[i + 1]->num_children; j++) {
        if (node->children[i + 1]->children[j]->node_type == AST_IDENTIFIER) {
          HashTableEntry_t *arg = lookup_identifier(node->children[i + 1]->children[j]->children[0]->node_value.str_val, false);
          if (arg->type_info.literal_type != tmp->args_list[j].type.literal_type
              && !can_coerce(arg->type_info.literal_type, tmp->args_list[j].type.literal_type)) {
            printf("Line %d: Argument \"%s\" has the wrong type.\n", yylineno, node->children[i + 1]->children[j]->children[0]->node_value.str_val);
            exit(ERR_WRONG_TYPE_ARGS);
          }
        }
        else if (node->children[i + 1]->children[j]->node_type == AST_LITERAL) {
          if (tmp->args_list[j].type.literal_type != node->children[i + 1]->children[j]->node_value.literal_type
            && !can_coerce(node->children[i + 1]->children[j]->node_value.literal_type, tmp->args_list[j].type.literal_type)) {
            printf("Line %d: Literal argument \"%s\" has the wrong type.\n", yylineno, node->children[i + 1]->children[j]->node_value.str_val);
            exit(ERR_WRONG_TYPE_ARGS);
          }
        }
        else if (node->children[i + 1]->children[j]->node_type == AST_PIPE_COMMANDS) {
          HashTableEntry_t *func = lookup_identifier(node->children[i + 1]->children[j]->children[0]->children[0]->node_value.str_val, false);
          if (func->type_info.literal_type != tmp->args_list[j].type.literal_type
              && !can_coerce(func->type_info.literal_type, tmp->args_list[j].type.literal_type)) {
              printf("Line %d: Argument \"%s\" (function) has the wrong type.\n", yylineno, node->children[i + 1]->children[j]->children[0]->children[0]->node_value.str_val);
              exit(ERR_WRONG_TYPE_ARGS);
          }
        }
        else if (node->children[i + 1]->children[j]->node_type == AST_PIPE_DOT) {
          if (i < 3) {
            printf("Line %d: Dot operator may not be used without a preceding function call.\n", yylineno);
            exit(ERR_WRONG_TYPE_ARGS);
          }
          else {
            HashTableEntry_t *last_func = lookup_identifier(node->children[i - 3]->children[0]->node_value.str_val, false);
            if (tmp->args_list[j].type.literal_type != last_func->type_info.literal_type
              && !can_coerce(last_func->type_info.literal_type, tmp->args_list[j].type.literal_type)) {
              printf("Line %d: Function \"%s\" has the wrong type for dot operator.\n", yylineno, node->children[i - 3]->children[0]->node_value.str_val);
              exit(ERR_WRONG_TYPE_ARGS);
            }
          }
        }

      }
    }

    if (has_infered_type) {
      node->infered_type = tmp->type_info.literal_type;
    }
  }
}


void register_identifier(HashTableEntry_t *entry) {
  if (entry == NULL)
    return;

  HashTableEntry_t *tmp = lookup_identifier_current_scope(entry->id, true);

  // check if it's already declared
  if (tmp != NULL) {
    printf("Line %d: Identifier \"%s\" already declared. ", yylineno, entry->id);
    printf("Previous declaration at line %d.\n", tmp->line_no);
    exit(ERR_DECLARED);
  }

  // add entry to highest scope hashtable
  ScopeEntry_t *highest_scope = TAILQ_FIRST(&scope);
  HASH_ADD_STR(highest_scope->hash_table, id, entry);

  // DEBUG: dump all scopes
  dump_scopes();
}

void dump_hashtable(HashTableEntry_t *htable) {
  HashTableEntry_t *s, *tmp;

  HASH_ITER(hh, htable, s, tmp) {
    printf("\n");

    printf(".id: %s\n", s->id);

    printf(".line_no: %d\n", s->line_no);

    if (s->type_info.is_vector)
      printf(".type_info.is_vector: true\n");
    else
      printf(".type_info.is_vector: false\n");

    if (s->type_info.is_static)
      printf(".type_info.is_static: true\n");
    else
      printf(".type_info.is_static: false\n");

    if (s->type_info.is_const)
      printf(".type_info.is_const: true\n");
    else
      printf(".type_info.is_const: false\n");

    switch(s->type_info.nature) {
      case N_CLASS_DECL:
        printf(".type_info.nature: class declaration\n");
        break;
      case N_FUNC_DECL:
        printf(".type_info.nature: function\n");
        break;
      case N_VARIABLE:
        printf(".type_info.nature: variable\n");
        break;
    }

    switch (s->type_info.literal_type) {
      case LT_INT:
        printf(".type_info.literal_type: int\n");
        break;
      case LT_FLOAT:
        printf(".type_info.literal_type: float\n");
        break;
      case LT_CHAR:
        printf(".type_info.literal_type: char\n");
        break;
      case LT_STRING:
        printf(".type_info.literal_type: string\n");
        break;
      case LT_BOOL:
        printf(".type_info.literal_type: bool\n");
        break;
      case LT_USER:
        printf(".type_info.literal_type: %s\n", s->type_info.class_name);
        break;
    }

    if (s->is_global)
      printf(".is_global: true\n");
    else
      printf(".is_global: false\n");

    if (s->start_label > 0)
      printf(".start_label: %d\n", s->start_label);

    printf(".offset_from_base_reg: %d\n", s->offset_from_base_reg);

    printf(".bytes_size: %d\n", s->bytes_size);

    printf(".list_length: %d\n", s->list_length);

    if (s->args_list != NULL) {
      printf(".args_list:\n");

      for (int i = 0; i < s->list_length; i++) {
        printf("  .id: %s\n", s->args_list[i].id);

        if (s->args_list[i].is_const)
          printf("  .type_info.is_const: true\n");
        else
          printf("  .type_info.is_const: false\n");

        switch (s->args_list[i].type.literal_type) {
          case LT_INT:
            printf("  .type_info.literal_type: int\n");
            break;
          case LT_FLOAT:
            printf("  .type_info.literal_type: float\n");
            break;
          case LT_CHAR:
            printf("  .type_info.literal_type: char\n");
            break;
          case LT_STRING:
            printf("  .type_info.literal_type: string\n");
            break;
          case LT_BOOL:
            printf("  .type_info.literal_type: bool\n");
            break;
          case LT_USER:
            printf("  .type_info.literal_type: class (%s)\n", s->args_list[i].type.class_name);
            break;
        }

        printf("  .bytes_size: %d\n\n", s->args_list[i].bytes_size);
      }
    }

    if (s->attributes_list != NULL) {
      printf(".attributes_list:\n");

      for (int i = 0; i < s->list_length; i++) {
        printf("  .id: %s\n", s->attributes_list[i].id);

        switch (s->attributes_list[i].encapsulation) {
          case ENC_PUBLIC:
            printf("  .encapsulation: public\n");
            break;
          case ENC_PROTECTED:
            printf("  .encapsulation: protected\n");
            break;
          case ENC_PRIVATE:
            printf("  .encapsulation: private\n");
            break;
        }

        switch (s->attributes_list[i].type.literal_type) {
          case LT_INT:
            printf("  .type_info.literal_type: int\n");
            break;
          case LT_FLOAT:
            printf("  .type_info.literal_type: float\n");
            break;
          case LT_CHAR:
            printf("  .type_info.literal_type: char\n");
            break;
          case LT_STRING:
            printf("  .type_info.literal_type: string\n");
            break;
          case LT_BOOL:
            printf("  .type_info.literal_type: bool\n");
            break;
          case LT_USER:
            printf("  .type_info.literal_type: class (%s)\n", s->type_info.class_name);
            break;
        }

        printf("  .bytes_size: %d\n\n", s->attributes_list[i].bytes_size);
      }
    }
  }
}

void push_scope() {
  ScopeEntry_t *new_scope = calloc(1, sizeof(ScopeEntry_t));
  new_scope->hash_table = NULL;
  TAILQ_INSERT_HEAD(&scope, new_scope, scopes);
}

void pop_scope() {
  if (TAILQ_EMPTY(&scope))
    return;

  // get the topmost scope
  ScopeEntry_t *top_scope = TAILQ_FIRST(&scope);

  // fully delete its hashtable
  HashTableEntry_t *cur_htable, *tmp;
  HASH_ITER(hh, top_scope->hash_table, cur_htable, tmp) {
    free(cur_htable->args_list);
    cur_htable->args_list = NULL;

    free(cur_htable->attributes_list);
    cur_htable->attributes_list = NULL;

    HASH_DEL(top_scope->hash_table, cur_htable);

    free(cur_htable);
    cur_htable = NULL;
  }

  // delete it from the stack
  TAILQ_REMOVE(&scope, top_scope, scopes);
  free(top_scope);
  top_scope = NULL;
}

void dump_scopes() {
  // DEBUG: uncomment for testing
  return;

  ScopeEntry_t *cur_scope;
  int level = 0;

  TAILQ_FOREACH_REVERSE(cur_scope, &scope, scope_s, scopes) {
    printf("\n");
    printf("-------------\n");
    printf("Scope level: %d\n", level);

    dump_hashtable(cur_scope->hash_table);

    printf("-------------\n");

    level += 1;
  }
}

int reserve_global_memory(int size) {
  int old_global_offset = current_global_offset;

  current_global_offset += size;

  return old_global_offset;
}

int reserve_local_memory(int size) {
  // get the topmost scope
  ScopeEntry_t *top_scope = TAILQ_FIRST(&scope);

  int old_global_offset = top_scope->base_register_offset;

  top_scope->base_register_offset += size;

  return old_global_offset;
}

int reserve_register() {
  int old_register = current_register_number;

  current_register_number += 1;

  return old_register;
}

int reserve_label() {
  int old_label = current_label_number;

  current_label_number += 1;

  return old_label;
}

struct code_s *create_code_segment() {
  struct code_s *segment = calloc(1, sizeof(struct code_s));

  // initialize intermediary code list
  TAILQ_INIT(segment);

  return segment;
}

struct code_s *add_instr_to_code_segment(
    struct code_s *segment,
    const char *instruction)
{
  // initialize segment if needed
  if (segment == NULL)
    segment = create_code_segment();

  // create code part
  CodePart_t *code_part = calloc(1, sizeof(CodePart_t));
  snprintf(code_part->instruction, 510, "%s", instruction);

  // add it to the code segment
  TAILQ_INSERT_TAIL(segment, code_part, codes);

  return segment;
}

struct code_s *concat_code_segments(
    struct code_s *s1,
    struct code_s *s2)
{
  if (s1 == NULL && s2 != NULL)
    return s2;

  if (s1 != NULL && s2 != NULL) {
    TAILQ_CONCAT(s1, s2, codes);
    free(s2);
  }

  return s1;
}
