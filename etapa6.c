#include <stdio.h>
#include "parser.tab.h"
#include "queue.h"

TAILQ_HEAD(scope_s, ScopeEntry_t) scope;

void *arvore = NULL;
void descompila (void *arvore);
void compila (void *arvore);
void libera (void *arvore);
extern int yylex_destroy(void);

int main (int argc, char **argv)
{
  // initialize scopes stack
  TAILQ_INIT(&scope);

  // create global scope
  ScopeEntry_t *global_scope = calloc(1, sizeof(ScopeEntry_t));
  global_scope->hash_table = NULL;
  TAILQ_INSERT_HEAD(&scope, global_scope, scopes);

  int ret = yyparse();

  compila(arvore);

  libera(arvore);
  arvore = NULL;
  yylex_destroy();

  return ret;
}