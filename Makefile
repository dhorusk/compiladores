BINARIES=etapa2 etapa3 etapa4 etapa5 etapa6
CC=gcc
FLEX=flex
BISON=bison
CFLAGS=-g -O2 -lfl

.PHONY: all clean $(BINARIES)

all: $(BINARIES)

lex.yy.c: scanner.l ast.h parser.tab.h
	$(FLEX) --yylineno $<

parser.tab.c: parser.y ast.h error.h uthash.h queue.h cdefs.h
	$(BISON) -v -d $<

$(BINARIES): % : %.c parser.tab.c parser.tab.h lex.yy.c ast.h
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	rm -r *.yy.c *.tab.* *.output *.o $(BINARIES) ; \
	find . -type f -name '*_out*.txt' -delete ; \
	find . -type f -name '*_mem*.txt' -delete ; \
	find . -type f -name '*_iloc*.txt' -delete ; \
	find . -type f -name '*_diff*.txt' -delete ; \
	find . -type f -name '*_valgrind*.txt' -delete